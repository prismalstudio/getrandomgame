/**
 *  @file		WorldMap.h
 *  @brief
 *  @details
 *  @author    	Emile
 *  @date      	2012-02-14
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 *
 *  Updates:
 *  	2013-08-28 emile added SFML, removes rendering, big refactor.
 */

#ifndef WORLDMAP_H_
#define WORLDMAP_H_

#include <string>

namespace Util {
template<typename T>
class TMatrix;
}

namespace Game {
class WorldCoord;
class RegionMap;
class AreaMap;
class Tile;

/*
 *
 */
class WorldMap {

public:

	/**
	 * @param name a string for the file/folder and name of the world.
	 * @param dirPath a string for the path to the world data.
	 */
	WorldMap(std::string name, std::string dirPath);
	virtual ~WorldMap();

	bool loadRegion(const Game::WorldCoord& coord);
	bool loadArea(const Game::WorldCoord& coord);

	/**
	 * @return a Game::Tile pointer or NULL of not loaded
	 */
	Game::Tile * getTile(const Game::WorldCoord& coord);
	/**
	 * @return a Game::AreaMap pointer or NULL of not loaded
	 */
	Game::AreaMap * getArea(const Game::WorldCoord& coord);
	/**
	 * @return a Game::RegionMap pointer or NULL of not loaded
	 */
	Game::RegionMap * getRegion(const Game::WorldCoord& coord);
	//Tile * GetTile(int xPos, int yPos, const WorldCoord * coord = NULL);

private:
	Util::TMatrix<Game::RegionMap*> * mRegionMap;
	std::string mWorldName;
	std::string mDirectoryPath; /*< path to the World dir of the game */
};

} /* namespace Game */
#endif /* WORLDMAP_H_ */
