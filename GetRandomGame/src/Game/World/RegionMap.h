/*
 * RegionMap.h
 *
 *  Created on: 2012-07-10
 *      Author: Emile
 */

#ifndef REGIONMAP_H_
#define REGIONMAP_H_

#include "WorldCoord.h"

namespace Util {
template<typename T>
class TMatrix;
}

namespace Game {
class WorldMap;
class AreaMap;

/*
 *
 */
class RegionMap {
public:
	RegionMap(Game::WorldMap * worldPointer, Game::WorldCoord coord);
	virtual ~RegionMap();
	virtual void Update(const int& frameDelay);
	//virtual void Draw(grg::Graphic& graphic);

	bool loadArea(const Game::WorldCoord& coord);

	Game::AreaMap * getArea(unsigned int x, unsigned int y) const;

protected:
	Game::WorldMap * mWorld;
	Util::TMatrix<AreaMap*> * mAreaMap;
	Game::WorldCoord mCoord;

};

} /* namespace Game */
#endif /* REGIONMAP_H_ */
