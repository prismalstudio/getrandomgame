/*
 * WorldCoord.cpp
 *
 *  Created on: 2012-08-02
 *      Author: Emile
 */

#include "WorldCoord.h"

#include "Util/StringExt.h"

#include <math.h>
#include <stdlib.h>

using Util::toStr;
using sf::Vector2i;
//#include <iostream>

namespace Game {
/**
 * Static max var initialization
 * default value is 0.
 */
unsigned int WorldCoord::mMaxRegionX = 0;
unsigned int WorldCoord::mMaxRegionY = 0;
unsigned int WorldCoord::mMaxAreaX = 0;
unsigned int WorldCoord::mMaxAreaY = 0;
unsigned int WorldCoord::mMaxTileX = 0;
unsigned int WorldCoord::mMaxTileY = 0;
unsigned int WorldCoord::mMaxTileSizeX = 0;
unsigned int WorldCoord::mMaxTileSizeY = 0;

WorldCoord::WorldCoord(int regionX, int regionY, int areaX, int areaY,
		int tileX, int tileY, int offsetX, int offsetY) {
	// set the coord
	set(regionX, regionY, areaX, areaY, tileX, tileY, offsetX, offsetY);
}

WorldCoord::WorldCoord(const Vector2i& regionPoint, const Vector2i& areaPoint,
		const Vector2i& tilePoint, const Vector2i& offsetPoint) {

	// set the coord
	set(regionPoint, areaPoint, tilePoint, offsetPoint);

}

WorldCoord::~WorldCoord() {
	// TODO Auto-generated destructor stub
}

/**
 * Static method to call at least once before using WorldCoord inside the game.
 */
void WorldCoord::setMaxBounds(unsigned int regionX, unsigned int regionY,
		unsigned int areaX, unsigned int areaY, unsigned int tileX,
		unsigned int tileY, unsigned int tileSizeX, unsigned int tileSizeY) {
	mMaxRegionX = regionX;
	mMaxRegionY = regionY;
	mMaxAreaX = areaX;
	mMaxAreaY = areaY;
	mMaxTileX = tileX;
	mMaxTileY = tileY;
	mMaxTileSizeX = tileSizeX;
	mMaxTileSizeY = tileSizeY;
}

/**
 * @return a string as (rX, rY | aX, aY | tX, tY | oX, oY)
 */
std::string WorldCoord::toString() const {
	// TODO streams are slow, think about sprintf or something.
	return "(" + toStr(getRegionX()) + ", " + toStr(getRegionY()) + " | "
			+ toStr(getAreaX()) + ", " + toStr(getAreaY()) + " | "
			+ toStr(getTileX()) + ", " + toStr(getTileY()) + " | "
			+ toStr(getOffsetX()) + ", " + toStr(getOffsetY()) + ")";
}

bool WorldCoord::operator ==(const WorldCoord& rhs) const {
	return (getOffset() == rhs.getOffset() ?
			(getTilePos() == rhs.getTilePos() ?
					(getAreaPos() == rhs.getAreaPos() ?
							(getRegionPos() == rhs.getRegionPos()) : false) :
					false) :
			false);
}

bool WorldCoord::operator !=(const WorldCoord& rhs) const {
	return !(*this == rhs);
}

bool WorldCoord::compareTile(const WorldCoord& rhs) const {
	return (getTilePos() == rhs.getTilePos() ?
			(getAreaPos() == rhs.getAreaPos() ?
					(getRegionPos() == rhs.getRegionPos()) : false) :
			false);
}

/**
 * GetAdjustedCoord
 *
 * Takes a World Coordinate object that may be out of bound
 * and tries to adjust its value so it's in bound of the map.
 *
 */
WorldCoord WorldCoord::getAdjustedCoord(const WorldCoord & source) const {
	WorldCoord newCoord(*this);
	newCoord.adjust(source);
	return newCoord;
}

WorldCoord WorldCoord::getAdjustedCoord(int regionX, int regionY, int areaX,
		int areaY, int tileX, int tileY, int offsetX, int offsetY) const {
	WorldCoord newCoord(*this);
	newCoord.adjust(regionX, regionY, areaX, areaY, tileX, tileY, offsetX,
			offsetY);
	return newCoord;
}

WorldCoord WorldCoord::getAdjustedCoordByOffset(const Vector2i & offset) const {
	WorldCoord newCoord(*this);
	newCoord.adjustOffset(offset);
	return newCoord;
}

void WorldCoord::adjust(int regionX, int regionY, int areaX, int areaY,
		int tileX, int tileY, int offsetX, int offsetY) {
	adjustRegionX(regionX);
	adjustRegionY(regionY);
	adjustAreaX(areaX);
	adjustAreaY(areaY);
	adjustTileX(tileX);
	adjustTileY(tileY);
	adjustOffsetX(offsetX);
	adjustOffsetY(offsetY);
}

void WorldCoord::adjust(const WorldCoord & coordMod) {
	adjustRegion(coordMod.getRegionPos());
	adjustArea(coordMod.getAreaPos());
	adjustTile(coordMod.getTilePos());
	adjustOffset(coordMod.getOffset());
}

/**
 * // set
 */
void WorldCoord::set(int regionX, int regionY, int areaX, int areaY, int tileX,
		int tileY, int offsetX, int offsetY) {
	setRegionPos(regionX, regionY);
	setAreaPos(areaX, areaY);
	setTilePos(tileX, tileY);
	setOffset(offsetX, offsetY);
}

void WorldCoord::set(const Vector2i & regionPoint, const Vector2i & areaPoint,
		const Vector2i & tilePoint, const Vector2i & offsetPoint) {
	setRegionPos(regionPoint);
	setAreaPos(areaPoint);
	setTilePos(tilePoint);
	setOffset(offsetPoint);
}

void WorldCoord::set(const WorldCoord & coord) {
	setRegionPos(coord.getRegionPos());
	setAreaPos(coord.getAreaPos());
	setTilePos(coord.getTilePos());
	setOffset(coord.getOffset());
}

///**
// * It's for output purposes, it gives you the TOP LEFT screen position
// * of the Tile pointed by this world coord.
// *
// * @param relCoord a screen coordinate relative to the world coordinate.
// * @return a Vector2i of the screen position of the Tile THIS coord points to.
// */
//sf::Vector2i WorldCoord::getTileOutputPosition(const sf::Vector2i& relCoord) {
//	return sf::Vector2i(relCoord.x - mTileOffset.x,
//			relCoord.y - 40 + mTileOffset.y);
//}
//
///**
// * Gets you the bottom-left corner of the Area.
// * @param relCoord a screen coordinate relative to the world coordinate.
// * @return a Vector2i of the screen position of the area THIS
// * world coord points to.
// */
//sf::Vector2i WorldCoord::getAreaRelativePosition(const sf::Vector2i& relCoord) {
//	return sf::Vector2i(
//			relCoord.x - (getMaxTileSizeX() * getTileX()) - mTileOffset.x,
//			relCoord.y + (getMaxTileSizeY() * getTileY()) + mTileOffset.y);
//}

/**
 * // getter MAX static 1 int
 */
unsigned int WorldCoord::getMaxAreaX() {
	return mMaxAreaX;
}

unsigned int WorldCoord::getMaxAreaY() {
	return mMaxAreaY;
}

unsigned int WorldCoord::getMaxRegionX() {
	return mMaxRegionX;
}

unsigned int WorldCoord::getMaxRegionY() {
	return mMaxRegionY;
}

unsigned int WorldCoord::getMaxTileX() {
	return mMaxTileX;
}

unsigned int WorldCoord::getMaxTileY() {
	return mMaxTileY;
}

unsigned int WorldCoord::getMaxTileSizeX() {
	return mMaxTileSizeX;
}

unsigned int WorldCoord::getMaxTileSizeY() {
	return mMaxTileSizeY;
}

sf::Vector2i WorldCoord::getTileSize() {
	return sf::Vector2i(mMaxTileSizeX, mMaxTileSizeY);
}

/**
 * // getter 1 int
 */
int WorldCoord::getRegionX() const {
	return mRegionPos.x;
}

int WorldCoord::getRegionY() const {
	return mRegionPos.y;
}

int WorldCoord::getAreaX() const {
	return mAreaPos.x;
}

int WorldCoord::getAreaY() const {
	return mAreaPos.y;
}

int WorldCoord::getTileX() const {
	return mTilePos.x;
}

int WorldCoord::getTileY() const {
	return mTilePos.y;
}

int WorldCoord::getOffsetX() const {
	return mTileOffset.x;
}

int WorldCoord::getOffsetY() const {
	return mTileOffset.y;
}

/**
 * // getter Vector2i
 */
Vector2i WorldCoord::getAreaPos() const {
	return mAreaPos;
}

Vector2i WorldCoord::getRegionPos() const {
	return mRegionPos;
}

Vector2i WorldCoord::getTilePos() const {
	return mTilePos;
}

Vector2i WorldCoord::getOffset() const {
	return mTileOffset;
}

/**
 * // setter 1 int
 * Auto-adjust to always be inside the max bounds
 */
void WorldCoord::setRegionX(int regionX) {

	int regionXMod = floor((double) regionX / getMaxRegionX());

	mRegionPos.x = (abs(regionXMod * getMaxRegionX() - regionX));
}

void WorldCoord::setRegionY(int regionY) {
	int regionYMod = floor((double) regionY / getMaxRegionY());

	mRegionPos.y = (abs(regionYMod * getMaxRegionY() - regionY));
}
void WorldCoord::setAreaX(int areaX) {
	// do the same for region
	int newRegionX = floor((double) areaX / getMaxAreaX());

	adjustRegionX(newRegionX);

	mAreaPos.x = (abs(newRegionX * getMaxAreaX() - areaX));

}

void WorldCoord::setAreaY(int areaY) {
	// do the same for region
	int newRegionY = floor((double) areaY / getMaxAreaY());

	adjustRegionY(newRegionY);

	mAreaPos.y = (abs(newRegionY * getMaxAreaY() - areaY));
}

void WorldCoord::setTileX(int tileX) {
	// first take the modifier of area from the tiles over/below max/min values
	int modAreaX = floor((double) tileX / getMaxTileX());
	adjustAreaX(modAreaX);

	// then calculate the key from what we have
	mTilePos.x = (abs(modAreaX * getMaxTileX() - tileX));
}

void WorldCoord::setTileY(int tileY) {
	// first take the modifier of area from the tiles over/below max/min values
	int modAreaY = floor((double) tileY / getMaxTileY());
	adjustAreaY(modAreaY);

	// then calculate the key from what we have
	mTilePos.y = (abs(modAreaY * getMaxTileY() - tileY));
}

void WorldCoord::setOffsetX(int offsetX) {
	// first take the modifier of area from the tiles over/below max/min values
	int modTileX = floor((double) offsetX / getMaxTileSizeX());
	adjustTileX(modTileX);

	// then calculate the key from what we have
	mTileOffset.x = (abs(modTileX * getMaxTileSizeX() - offsetX));
}

void WorldCoord::setOffsetY(int offsetY) {
	// first take the modifier of area from the tiles over/below max/min values
	int modTileY = floor((double) offsetY / getMaxTileSizeY());
	adjustTileY(modTileY);

	// then calculate the key from what we have
	mTileOffset.y = (abs(modTileY * getMaxTileSizeY() - offsetY));
}

/**
 * // setter 2 int
 */
void WorldCoord::setRegionPos(int regionX, int regionY) {
	setRegionX(regionX);
	setRegionY(regionY);
}

void WorldCoord::setAreaPos(int areaX, int areaY) {
	setAreaX(areaX);
	setAreaY(areaY);
}

void WorldCoord::setTilePos(int tileX, int tileY) {
	setTileX(tileX);
	setTileY(tileY);
}

void WorldCoord::setOffset(int offsetX, int offsetY) {
	setOffsetX(offsetX);
	setOffsetY(offsetY);
}

/**
 * // setter Vector2i
 */
void WorldCoord::setRegionPos(const Vector2i & regionPos) {
	setRegionX(regionPos.x);
	setRegionY(regionPos.y);
}

void WorldCoord::setAreaPos(const Vector2i & areaPos) {
	setAreaX(areaPos.x);
	setAreaY(areaPos.y);
}

void WorldCoord::setTilePos(const Vector2i & tilePos) {
	setTileX(tilePos.x);
	setTileY(tilePos.y);
}

void WorldCoord::setOffset(const Vector2i & offsetPos) {
	setOffsetX(offsetPos.x);
	setOffsetY(offsetPos.y);
}

/**
 * // adjust 1 int
 */

void WorldCoord::adjustRegionX(int regionXmod) {
	regionXmod += getRegionX();
	setRegionX(regionXmod);
}

void WorldCoord::adjustRegionY(int regionYmod) {
	regionYmod += getRegionY();
	setRegionY(regionYmod);
}

void WorldCoord::adjustAreaX(int areaXmod) {
	areaXmod += getAreaX();
	setAreaX(areaXmod);
}

void WorldCoord::adjustAreaY(int areaYmod) {
	areaYmod += getAreaY();
	setAreaY(areaYmod);
}

void WorldCoord::adjustTileX(int tileXmod) {
	tileXmod += getTileX();
	setTileX(tileXmod);
}

void WorldCoord::adjustTileY(int tileYmod) {
	tileYmod += getTileY();
	setTileY(tileYmod);
}

void WorldCoord::adjustOffsetX(int offsetXmod) {
	offsetXmod += getOffsetX();
	setOffsetX(offsetXmod);
}

void WorldCoord::adjustOffsetY(int OffsetYmod) {
	OffsetYmod += getOffsetY();
	setOffsetY(OffsetYmod);
}

/**
 * // adjust 2 int
 */
void WorldCoord::adjustRegion(int regionXmod, int regionYmod) {
	adjustRegionX(regionXmod);
	adjustRegionY(regionYmod);
}

void WorldCoord::adjustArea(int areaXmod, int areaYmod) {
	adjustAreaX(areaXmod);
	adjustAreaY(areaYmod);
}

void WorldCoord::adjustTile(int tileXmod, int tileYmod) {
	adjustTileX(tileXmod);
	adjustTileY(tileYmod);
}

void WorldCoord::adjustOffset(int offsetXmod, int offsetYmod) {
	adjustOffsetX(offsetXmod);
	adjustOffsetY(offsetYmod);
}

/**
 * // adjust Vector2i
 */
void WorldCoord::adjustRegion(const Vector2i & regionPos) {
	adjustRegionX(regionPos.x);
	adjustRegionY(regionPos.y);
}

void WorldCoord::adjustArea(const Vector2i & areaPos) {
	adjustAreaX(areaPos.x);
	adjustAreaY(areaPos.y);
}

void WorldCoord::adjustTile(const Vector2i & tilePos) {
	adjustTileX(tilePos.x);
	adjustTileY(tilePos.y);
}

void WorldCoord::adjustOffset(const Vector2i& offsetPos) {
	adjustOffsetX(offsetPos.x);
	adjustOffsetY(offsetPos.y);
}

} /* namespace Game */
