/*
 * AreaMap.cpp
 *
 *  Created on: 2012-07-10
 *      Author: Emile
 */

#include "AreaMap.h"
#include "Tile.h"
#include "WorldMap.h"

#include "GRGEngine/Graphic/Sprite.h"

#include "Util/TMatrix.h"

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

namespace Game {

AreaMap::AreaMap(Game::WorldMap * worldPointer, WorldCoord coord) :
				mWorld(worldPointer),
				mCoord(coord) {

	mTileMap = new (std::nothrow) Util::TMatrix<Tile*>(
			WorldCoord::getMaxTileX(), WorldCoord::getMaxTileY());
	mTileMap->fill(NULL); // Fill and initialise with NULL

	//Load(); // en attendant, TEST
}

AreaMap::~AreaMap() {
	// TODO Auto-generated destructor stub
}

void AreaMap::loadTile() {
	int randomNumber = 0;

	grg::Sprite tileset("asset/image/tileset_trans_sfml.png");

	for (unsigned int i = 0; i < mTileMap->rows(); i++) {
		for (unsigned int j = 0; j < mTileMap->cols(); j++) {

			// makes it a % perfect tile, the rest is imperfect tile
			randomNumber = ((rand() % 100) < 25) ? 4 : (rand() % 4);

			Tile* newTile = new (std::nothrow) Tile(mWorld,
					WorldCoord(mCoord.getRegionX(), mCoord.getRegionY(),
							mCoord.getAreaX(), mCoord.getAreaY(), i, j), 2, 3,
					randomNumber);
			if (!newTile) {
				cerr << "AreaMap::loadTile(" << i << "," << j
						<< ")::ERROR loading tile.." << endl;
				continue; // skip the set below, go to the next tile
			}
			newTile->setSprite(tileset);
			mTileMap->set(i, j, newTile);

		}
	}
}

void AreaMap::update(const int& delta) {
}

Game::Tile * AreaMap::getTile(unsigned int x, unsigned int y) const {
	return mTileMap->at(x, y);
}

const Game::WorldCoord& AreaMap::getCoord() const {
	return mCoord;
}

} /* namespace Game */
