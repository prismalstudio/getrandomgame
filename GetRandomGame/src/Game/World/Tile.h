/*
 * Tile.h
 *
 *  Created on: 2012-07-10
 *      Author: Emile
 */

#ifndef TILE_H_
#define TILE_H_

#include "WorldCoord.h"

#include "GRGEngine/Graphic/Sprite.h"

namespace grg
{
class Graphic;
}

namespace Game
{
class WorldMap;
class EntityManager;
/*
 *
 */
class Tile
{
	public:
		//Tile();
		Tile(Game::WorldMap * world, const WorldCoord& coord, short tileType,
				short totalType, unsigned char randomMod);
		virtual ~Tile();
		void update(const int& deltaTime);

		void draw(grg::Graphic& graphic);

		void setPosition(float x, float y);

		void SetTileType(int tileType); // uses auto-tiling
		int GetCornerBit(unsigned int cornerKey) const;
		void SetCornerBit(unsigned int cornerKey, int bit);
		int GetTileKey(int tileType) const;
		int getBaseType() const;
		const Game::WorldCoord& getCoord() const;
		int getRandomMod() const;
		void setBaseType(int baseType);
		void setRandomMod(int randomMod);

		const grg::Sprite& getSprite() const;
		void setSprite(const grg::Sprite& spriteSheet);

	protected:
		Game::WorldMap * mWorld;
		Game::WorldCoord mCoord;
		short mBaseType;
		short mTotalType;
		unsigned char mRandomMod;
		int mCornerTypeBit[4];

		grg::Sprite mSpriteSheet;

		EntityManager *mEntityManager;

};

} /* namsepace Game */
#endif /* TILE_H_ */
