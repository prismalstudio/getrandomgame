/*
 * AreaMap.h
 *
 *  Created on: 2012-07-10
 *      Author: Emile
 */

#ifndef AREAMAP_H_
#define AREAMAP_H_

#include "WorldCoord.h"

namespace Util {
template<typename T>
class TMatrix;
}

namespace Game {
class WorldMap;
class Tile;
/*
 *
 */
class AreaMap {
public:
	AreaMap(Game::WorldMap * worldPointer, WorldCoord coord);
	virtual ~AreaMap();
	virtual void loadTile();
	void update(const int& delta);

	Game::Tile * getTile(unsigned int x, unsigned int y) const;

	const Game::WorldCoord& getCoord() const;

protected:
	Game::WorldMap * mWorld;
	Util::TMatrix<Game::Tile*> * mTileMap; // TODO maybe not Tile* but plain Tile ob
	WorldCoord mCoord;

};

} /* namespace Game */

#endif /* AREAMAP_H_ */
