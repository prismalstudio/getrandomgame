/*
 * RegionMap.cpp
 *
 *  Created on: 2012-07-10
 *      Author: Emile
 */

#include "RegionMap.h"

#include "AreaMap.h"
#include "WorldMap.h"

#include "Util/TMatrix.h"

#include <iostream> // cout, cerr, endl
using std::cout;
using std::endl;
using std::cerr;

namespace Game {
RegionMap::RegionMap(WorldMap * worldPointer, WorldCoord coord) :
				mWorld(worldPointer),
				mCoord(coord) {
	//std::cout << "RegionMap()" << std::endl;
	mAreaMap = new (std::nothrow) Util::TMatrix<AreaMap*>(
			WorldCoord::getMaxAreaX(), WorldCoord::getMaxAreaY());
	mAreaMap->fill(NULL); // Fill and initialise with NULL

	//std::cout << "RegionMap()::end" << std::endl;
}

RegionMap::~RegionMap() {

}

bool RegionMap::loadArea(const Game::WorldCoord& coord) {
	unsigned int x(coord.getAreaX());
	unsigned int y(coord.getAreaY());
	//cout << "RegionMap::LoadArea(" << x << ", " << y << ")" << endl;
	// if not Out of bound
	if (mAreaMap->isInsideBounds(x, y)) {
		// if not already loaded
		if (mAreaMap->at(x, y) == NULL) {
			AreaMap * newArea = new AreaMap(mWorld,
					WorldCoord(mCoord.getRegionX(), mCoord.getRegionY(), x, y));

			if (!newArea) {
				cerr << "RegionMap::LoadArea(" << x << ", " << y
						<< ")::ERROR: not loaded properly..." << endl;
				return false;
			}
			newArea->loadTile();
			mAreaMap->set(x, y, newArea);
		}
//		else {
//			cout << "RegionMap::LoadArea(" << x << ", " << y
//					<< ") already loaded." << endl;
//		}
	} else {
		cerr << "RegionMap::LoadArea(" << x << ", " << y
				<< ")::ERROR: out of bound..." << endl;
		return false;
	}

	return true;
}

void RegionMap::Update(const int& frameDelay) {
}

Game::AreaMap * RegionMap::getArea(unsigned int x, unsigned int y) const {
	return mAreaMap->at(x, y);
}

} /* namespace Game */
