/*
 * WorldMap.cpp
 *
 *  Created on: 2012-02-14
 *      Author: Emile
 */

#include "WorldMap.h"

#include "Tile.h"
#include "AreaMap.h"
#include "RegionMap.h"

#include "Util/TMatrix.h"

#include <iostream>
//#include <fstream>
//#include <math.h>

using std::cout;
using std::endl;
using std::cerr;

namespace Game {

WorldMap::WorldMap(std::string name, std::string dirPath) {

	mWorldName = name;
	mDirectoryPath = dirPath;
	// init the RegionMap
	mRegionMap = new (std::nothrow) Util::TMatrix<RegionMap*>(
			WorldCoord::getMaxRegionX(), WorldCoord::getMaxRegionY());
	mRegionMap->fill(NULL); // Fill and initialise with NULL
}

WorldMap::~WorldMap() {
}

/**
 * @return true if everything went fine, region is loaded. False otherwise.
 */
bool WorldMap::loadRegion(const Game::WorldCoord& coord) {
	unsigned int x(coord.getRegionX());
	unsigned int y(coord.getRegionY());
	//cout << "WorldMap::LoadRegion(" << x << ", " << y << ")" << endl;

	// if inside bounds and not loaded yet
	if (mRegionMap->isInsideBounds(x, y)) {
		if (mRegionMap->at(x, y) == NULL) {
			// load the new region
			RegionMap * newRegion = new (std::nothrow) RegionMap(this, coord);

			// make sure the region is loaded
			if (!newRegion) {
				// output an error otherwise and return false;
				cerr << "WorldMap::LoadRegion(" << coord.getRegionX() << ", "
						<< coord.getRegionY()
						<< ")::ERROR, not loaded properly..." << endl;
				return false;
			}

			// then set it
			mRegionMap->set(x, y, newRegion);

		}
//		else {
//			// already loaded
//			cout << "WorldMap::LoadRegion(" << x << ", " << y
//					<< ") is already loaded" << endl;
//		}
	} else {
		// the coord is out of bounds for some reason.
		cerr << "WorldMap::LoadRegion(" << x << ", " << y
				<< ")::ERROR, out of bound..." << endl;
		return false;
	}
	return true;
}

/**
 * @return true if everything went fine, area is loaded. False otherwise.
 */
bool WorldMap::loadArea(const Game::WorldCoord& coord) {
	if (loadRegion(coord)) {
		return getRegion(coord)->loadArea(coord);
	}
	return true;
}

/**
 * GetTile
 * return tile...
 */
Game::Tile *WorldMap::getTile(const WorldCoord & coord) {
	//cout << "WorldMap::getTile" << coord.toString() << endl;
// prevent problem while parts of map aren't loaded yet
	if (!getRegion(coord)) {
		//return NULL;
		loadRegion(coord); // debug
	}
	if (!getArea(coord)) {
		//return NULL;
		loadArea(coord); // debug
	}

	if (!getArea(coord)->getTile(coord.getTileX(), coord.getTileY())) {
		return NULL;
	}

	return mRegionMap->at(coord.getRegionX(), coord.getRegionY())->getArea(
			coord.getAreaX(), coord.getAreaY())->getTile(coord.getTileX(),
			coord.getTileY());
}

Game::AreaMap* WorldMap::getArea(const WorldCoord& coord) {
	//cout << "WorldMap::getArea" << coord.toString() << endl;
	if (!getRegion(coord)) {
		//return NULL;
		loadRegion(coord); // debug
	}
	if (!getRegion(coord)->loadArea(coord)) {
		return NULL; // debug
	}

	return mRegionMap->at(coord.getRegionX(), coord.getRegionY())->getArea(
			coord.getAreaX(), coord.getAreaY());

}

Game::RegionMap* WorldMap::getRegion(const Game::WorldCoord& coord) {
	//cout << "WorldMap::getRegion" << coord.toString() << endl;
	return mRegionMap->at(coord.getRegionX(), coord.getRegionY());
}

} /* namespace Game */
