/*
 * Tile.cpp
 *
 *  Created on: 2012-07-10
 *      Author: Emile
 */

#include "Tile.h"

#include "WorldMap.h"

#include "GRGEngine/Graphic/Graphic.h"

#include <iostream>

using std::cout;
using std::endl;
using std::cerr;

namespace Game {

Tile::Tile(Game::WorldMap * world, const WorldCoord& coord, short tileType,
		short totalType, unsigned char randomMod) :
				mWorld(world),
				mCoord(coord),
				mBaseType(tileType),
				mTotalType(totalType),
				mRandomMod(randomMod),
				mEntityManager(NULL)
				{

	mCornerTypeBit[0] = tileType;
	mCornerTypeBit[1] = tileType;
	mCornerTypeBit[2] = tileType;
	mCornerTypeBit[3] = tileType;

}

Tile::~Tile() {
}

void Tile::update(const int& deltaTime) {
}

void Tile::draw(grg::Graphic& graphic) {
	sf::Vector2i tileSize = WorldCoord::getTileSize();
	sf::IntRect texRect(sf::Vector2i(0, 0), tileSize);
	int keyX;
	bool isTypePresent = false;
	for (int curType = mBaseType; curType < mTotalType; ++curType) {

		// Make sure the type is on the tile
		for (int cIndex = 0; cIndex <= 3; cIndex++) {
			if (GetCornerBit(cIndex) == curType) {
				isTypePresent = true;
				break;
			}
		}
		// if not, skip it
		if (!isTypePresent) {
			continue;
		}

		// then get is key
		keyX = GetTileKey(curType);

		// we shouldn't come to this, but if that type happens to
		// be empty on that tile, don't draw it, so skip it.
		if (keyX == 0) {
			continue;
		}

		// If it's a full tile, add a little random to it
		// so it looks better.
		if (keyX == 15) {
			keyX += mRandomMod;
		}

		// then consequently adjust the rect to grab the right tile
		// from the tileset (sprite)

		texRect.left = keyX * tileSize.x;
		texRect.top = curType * tileSize.y;

		mSpriteSheet.setTextureRect(texRect);
		graphic.draw(mSpriteSheet);

	}
} // end of draw

void Tile::setPosition(float x, float y) {
	mSpriteSheet.setPosition(x, y);
}
/**
 * Gets the tile key inside the tileset. (for auto-tiling)
 *
 * takes each corner and adds it up accordingly.
 * -----------
 * | b0 | b1 |
 * -----------
 * | b2 | b3 |
 * -----------
 *
 * e.g.:
 * -----------
 * | 0  | 1  |
 * -----------
 * | 1  | 1  |
 * -----------
 *
 * b3b2b1b0 = 1110 = 14
 *
 * @param tileType a int tile type
 * @return a int of the key
 *
 */
int Tile::GetTileKey(int tileType) const {
	int tileKey = 0;
	int cpt = 0;

	// Binary value loop, convert binary to base 10
	// i is 1, 2, 4, 8
	for (int i = 1; i <= 8; i *= 2) {

//		tileKey += (mCornerTypeBit[cpt] != 0)
//				* (mCornerTypeBit[cpt] >= tileType) * i;

		// awesome calculation of binary
		tileKey += (mCornerTypeBit[cpt] >= tileType) * i;
		++cpt; // goes from 0 to 3 for the four corners
	}
	return tileKey;
}

/**
 * For auto-tiling, gives you the bit of a corner.
 * @param cornerKey an unsigned int, index of the corner.
 * @return an int, the type of the corner.
 */
int Tile::GetCornerBit(unsigned int cornerKey) const {
	if (cornerKey < 4) {
		return mCornerTypeBit[cornerKey];
	}
	// default returned value
	return mCornerTypeBit[0];
}

void Tile::SetCornerBit(unsigned int cornerKey, int bit) {
	if (cornerKey < 4) { // prevent OOB
		mCornerTypeBit[cornerKey] = bit;

		// if each 4 corners are at least equal or higher
		// than each others for the new bit type,
		// set the base type to the new bit.
		if (GetTileKey(bit) == 15) {
			mBaseType = bit;
		}
	}
}

void Tile::SetTileType(int tileType) {
	this->mBaseType = tileType;
	Game::Tile * curTile;

	int startx = 0, starty = 0, keyCorner;

	// Loop the tile 4 corners
	for (int keyYmod = 0; keyYmod >= -1; keyYmod--) {
		starty += keyYmod;
		startx = 0;
		for (int keyXmod = 0; keyXmod <= 1; keyXmod++) {
			startx += keyXmod;

//			keyCorner = 0; // reset the corner key
//
//			int highessTypeValue = tileType;
//			// find the highess tile value around the corner
//			for (int y = starty; y <= starty + 1; y++) {
//				for (int x = startx; x >= startx - 1; x--) {
//					// get the Tile
//					WorldCoord curCoord = mCoord.Modify(0, 0, 0, 0, x, y);
//					int currentTileType = mWorld->GetTile(&curCoord)->GetTileType();
//					if (currentTileType > highessTypeValue) {
//						highessTypeValue = currentTileType;
//					}
//					keyCorner++;
//
//				}
//			}

			keyCorner = 0; // reset the corner key

			// Apply the highess tile value to that corner
			// by looping the 4 tiles around that corner
			for (int y = starty; y <= starty + 1; y++) {
				for (int x = startx; x >= startx - 1; x--) {
					// get the Tile
					WorldCoord curCoord = mCoord.getAdjustedCoord(0, 0, 0, 0, x,
							y);
					curTile = mWorld->getTile(curCoord);

					// apply the highess type value for the corner
					curTile->SetCornerBit(keyCorner, tileType);

					keyCorner++;
				}
			} // end of that corner indice/key loop

		} // fin keyXmod
	} // fin keyYmod

}

/**
 * GETTER SETTER
 */

int Tile::getBaseType() const {
	return mBaseType;
}

const Game::WorldCoord& Tile::getCoord() const {
	return mCoord;
}

int Tile::getRandomMod() const {
	return mRandomMod;
}

void Tile::setBaseType(int baseType) {
	mBaseType = baseType;
}

void Tile::setRandomMod(int randomMod) {
	mRandomMod = randomMod;
}

const grg::Sprite& Tile::getSprite() const {
	return mSpriteSheet;
}

void Tile::setSprite(const grg::Sprite& spriteSheet) {
	mSpriteSheet = spriteSheet;
}

} /* namespace Game */
