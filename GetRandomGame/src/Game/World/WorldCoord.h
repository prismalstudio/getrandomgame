/**
 *  @file		WorldCoord.h
 *  @brief		Coordinate in the game world
 *  @details	The world is a set a Region which are a set
 *  			of area which are a set of tile. Add an offset
 *  			within the tile and you have a WorldCoord.
 *
 *  @author    	E. B.
 *  @date      	2013-03-28 (Created on 2012-08-02)
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#ifndef WORLDCOORD_H_
#define WORLDCOORD_H_

#include <SFML/System/Vector2.hpp>

#include <string>

namespace Game {
/*
 *
 */
class WorldCoord {
public:
	WorldCoord(int regionX = 0, int regionY = 0, int areaX = 0, int areaY = 0,
			int tileX = 0, int tileY = 0, int offsetX = 0, int offsetY = 0);
	WorldCoord(const sf::Vector2i& regionPoint, const sf::Vector2i& areaPoint,
			const sf::Vector2i& tilePoint, const sf::Vector2i& offsetPoint);
	virtual ~WorldCoord();

	// custom methods
	static void setMaxBounds(unsigned int regionX, unsigned int regionY,
			unsigned int areaX, unsigned int areaY, unsigned int tileX,
			unsigned int tileY, unsigned int tileSizeX, unsigned int tileSizeY);

	std::string toString() const;

	bool operator==(const WorldCoord& rhs) const;
	bool operator!=(const WorldCoord& rhs) const;

	bool compareTile(const WorldCoord& rhs) const;

	// read/use only
	WorldCoord getAdjustedCoord(int regionX = 0, int regionY = 0, int areaX = 0,
			int areaY = 0, int tileX = 0, int tileY = 0, int offsetX = 0,
			int offsetY = 0) const;
	WorldCoord getAdjustedCoord(const WorldCoord& coordMod) const;
	WorldCoord getAdjustedCoordByOffset(const sf::Vector2i& offset) const;

	// adjust
	void adjust(int regionX = 0, int regionY = 0, int areaX = 0, int areaY = 0,
			int tileX = 0, int tileY = 0, int offsetX = 0, int offsetY = 0);
	void adjust(const WorldCoord& coordMod);

	// set
	void set(int regionX, int regionY, int areaX, int areaY, int tileX,
			int tileY, int offsetX = 0, int offsetY = 0);
	void set(const sf::Vector2i& regionPoint, const sf::Vector2i& areaPoint,
			const sf::Vector2i& tilePoint, const sf::Vector2i& offsetPoint);
	void set(const WorldCoord& coord);

	// screen coordinate getter
//	sf::Vector2i getTileOutputPosition(const sf::Vector2i& relCoord);
//	sf::Vector2i getAreaRelativePosition(const sf::Vector2i& relCoord);

// getter MAX static 1 int
	static unsigned int getMaxAreaX();
	static unsigned int getMaxAreaY();
	static unsigned int getMaxRegionX();
	static unsigned int getMaxRegionY();
	static unsigned int getMaxTileX();
	static unsigned int getMaxTileY();
	static unsigned int getMaxTileSizeX();
	static unsigned int getMaxTileSizeY();
	static sf::Vector2i getTileSize();

	// getter 1 int
	int getRegionX() const;
	int getRegionY() const;
	int getAreaX() const;
	int getAreaY() const;
	int getTileX() const;
	int getTileY() const;
	int getOffsetX() const;
	int getOffsetY() const;

	// getter point2i
	sf::Vector2i getRegionPos() const;
	sf::Vector2i getAreaPos() const;
	sf::Vector2i getTilePos() const;
	sf::Vector2i getOffset() const;

	// setter 1 int
	void setRegionX(int regionX);
	void setRegionY(int regionY);
	void setAreaX(int areaX);
	void setAreaY(int areaY);
	void setTileX(int tileX);
	void setTileY(int tileY);
	void setOffsetX(int offsetX);
	void setOffsetY(int offsetY);

	// setter 2 int
	void setRegionPos(int regionX, int regionY);
	void setAreaPos(int areaX, int areaY);
	void setTilePos(int tileX, int tileY);
	void setOffset(int offsetX, int offsetY);

	// setter point2i
	void setRegionPos(const sf::Vector2i& regionPos);
	void setAreaPos(const sf::Vector2i& areaPos);
	void setTilePos(const sf::Vector2i& tilePos);
	void setOffset(const sf::Vector2i& offsetPos);

	// adjust 1 int
	void adjustRegionX(int regionXmod);
	void adjustRegionY(int regionYmod);
	void adjustAreaX(int areaXmod);
	void adjustAreaY(int areaYmod);
	void adjustTileX(int tileXmod);
	void adjustTileY(int tileYmod);
	void adjustOffsetX(int offsetXmod);
	void adjustOffsetY(int OffsetYmod);

	// adjust 2 int
	void adjustRegion(int regionXmod, int regionYmod);
	void adjustArea(int areaXmod, int areaYmod);
	void adjustTile(int tileXmod, int tileYmod);
	void adjustOffset(int offsetXmod, int offsetYmod);

	// adjust point2i
	void adjustRegion(const sf::Vector2i& regionPos);
	void adjustArea(const sf::Vector2i& areaPos);
	void adjustTile(const sf::Vector2i& tilePos);
	void adjustOffset(const sf::Vector2i& offsetPos);

private:
	sf::Vector2i mRegionPos;
	sf::Vector2i mAreaPos;
	sf::Vector2i mTilePos;
	sf::Vector2i mTileOffset;
	static unsigned int mMaxRegionX;
	static unsigned int mMaxRegionY;
	static unsigned int mMaxAreaX;
	static unsigned int mMaxAreaY;
	static unsigned int mMaxTileX;
	static unsigned int mMaxTileY;
	static unsigned int mMaxTileSizeX;
	static unsigned int mMaxTileSizeY;
};

} /* namespace Game */
#endif /* WORLDCOORD_H_ */
