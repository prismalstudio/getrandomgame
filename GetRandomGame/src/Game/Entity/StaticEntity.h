/*
 * StaticEntity.h
 *
 *  Created on: 2013-01-30
 *      Author: bebleu
 */

#ifndef STATICENTITY_H_
#define STATICENTITY_H_

#include "GameEntity.h"

namespace Game{

class StaticEntity: public GameEntity {
public:
	StaticEntity();
	StaticEntity(int id);
	virtual ~StaticEntity();

	virtual void update(const int & frameDelay) =0;
	virtual void draw() =0;
	virtual bool init() =0;
	virtual void close() =0;
};


} //namespace Game
#endif /* STATICENTITY_H_ */
