/*
 * MovingEntity.h
 *
 *  Created on: 2013-01-30
 *      Author: bebleu
 */

#ifndef MOVINGENTITY_H_
#define MOVINGENTITY_H_

#include "GameEntity.h"

namespace grg
{
	class Graphic;
}

namespace Game{
class MovingEntity: public GameEntity
{
public:
	MovingEntity();
	MovingEntity(int id);
	virtual ~MovingEntity();

	virtual void update(const int & frameDelay) =0;
	virtual void draw(grg::Graphic& graphic) =0;
	virtual bool init() =0;
	virtual void close() =0;

};

}//namespace Game{

#endif /* MOVINGENTITY_H_ */
