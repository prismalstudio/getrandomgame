/*
 * VirtualObject.h
 *
 *  Created on: 2013-01-30
 *      Author: bebleu
 */

#ifndef VIRTUALOBJECT_H_
#define VIRTUALOBJECT_H_

#include <SFML/System/Vector2.hpp>

namespace Game {

class VirtualObject {
public:
	VirtualObject();
	virtual ~VirtualObject();

	sf::Vector2f getSize() const;
	void setSize(sf::Vector2f size);

private:
	sf::Vector2f mSize;
};

} //namespace Game
#endif /* VIRTUALOBJECT_H_ */

