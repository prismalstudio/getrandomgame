/*
 * PlayerEntityVO.h
 *
 *  Created on: 2013-02-03
 *      Author: bebleu
 */

#ifndef PLAYERENTITYVO_H_
#define PLAYERENTITYVO_H_

#include "MovingEntityVO.h"

namespace Game{

class PlayerEntityVO: public MovingEntityVO {
public:
	PlayerEntityVO();
	virtual ~PlayerEntityVO();
};

}//namespace Game

#endif /* PLAYERENTITYVO_H_ */
