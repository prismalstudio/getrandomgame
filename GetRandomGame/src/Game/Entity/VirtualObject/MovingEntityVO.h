/*
 * MovingEntityVO.h
 *
 *  Created on: 2013-01-30
 *      Author: bebleu
 */

#ifndef MOVINGENTITYVO_H_
#define MOVINGENTITYVO_H_

#include "VirtualObject.h"

namespace Game{

class MovingEntityVO: public VirtualObject {
public:
	MovingEntityVO();
	virtual ~MovingEntityVO();

	float getSpeed() const;
	void setSpeed(float speed);

private:
	float mSpeed;

};

}//namespace Game

#endif /* MOVINGENTITYVO_H_ */
