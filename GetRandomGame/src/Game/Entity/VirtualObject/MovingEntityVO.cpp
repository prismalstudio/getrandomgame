/*
 * MovingEntityVO.cpp
 *
 *  Created on: 2013-01-30
 *      Author: bebleu
 */

#include "MovingEntityVO.h"

namespace Game {

MovingEntityVO::MovingEntityVO() {
	mSpeed = 0.0f;

}

MovingEntityVO::~MovingEntityVO() {
	// TODO Auto-generated destructor stub
}

float MovingEntityVO::getSpeed() const {
	return mSpeed;
}

void MovingEntityVO::setSpeed(float speed) {
	mSpeed = speed;
}

} //namespace Game
