/*
 * PlayerEntity.h
 *
 *  Created on: 2013-01-30
 *      Author: bebleu
 */

#ifndef PLAYERENTITY_H_
#define PLAYERENTITY_H_

#include "MovingEntity.h"

#include <SFML/Graphics/RenderWindow.hpp>
#include <Game/Action/Handler/CharacterActionHandler.h>

namespace grg
{
	class GuiContainer;
	class GTextArea;
	class Graphic;
	class Event;
}

namespace Game
{

class PlayerEntityVO;
class PlayerEntity: public MovingEntity
{
public:
	PlayerEntity();
	virtual ~PlayerEntity();

	virtual void update(const int & frameDelay);
	virtual void draw(grg::Graphic& graphic);
	virtual bool init();
	virtual void close();

	virtual bool moveForward();
	virtual bool moveBackward();
	virtual bool moveLeft();
	virtual bool moveRight();
	virtual bool use();
	virtual bool jump();

	void move(int xDirection, int yDirection);

private:
	PlayerEntityVO * playerVO;

};

}//namespace Game
#endif /* PLAYERENTITY_H_ */
