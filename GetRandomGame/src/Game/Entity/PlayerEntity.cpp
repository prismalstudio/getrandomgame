/*
 * PlayerEntity.cpp
 *
 *  Created on: 2013-01-30
 *      Author: bebleu
 */

#include "PlayerEntity.h"
#include "Game/Entity/VirtualObject/PlayerEntityVO.h"
#include "GRGEngine/Graphic/Sprite.h"
#include "GRGEngine/Graphic/GraphicBehavior/AnimationBehavior.h"

using grg::Sprite;

namespace Game
{

PlayerEntity::PlayerEntity()
{

	playerVO = new PlayerEntityVO();
	playerVO->setSpeed(20);
	playerVO->setSize(sf::Vector2f(20, 20));
	mSprite = Sprite("asset/image/cat.png", sf::IntRect(0, 0, 95, 120));
	//mRenderStates = new sf::RenderStates();

//	playerVO = NULL;
//	mGuiContainer	= new (std::nothrow) grg::GuiContainer();
//	mDebugText		= new (std::nothrow) grg::GTextArea();
}

PlayerEntity::~PlayerEntity()
{

}

bool PlayerEntity::init()
{

	playerVO = new PlayerEntityVO();
	playerVO->setSpeed(3);
	mSprite = grg::Sprite("asset/image/cat.png", sf::IntRect(0, 0, 95, 120));
	mSprite.pushBehavior(new grg::AnimationBehavior(250, 4));

	return true;
}

void PlayerEntity::update(const int& frameDelay)
{
	//mSprite.setPosition(mCurrentPosition.x, mCurrentPosition.y);
	mSprite.Update(frameDelay);
}

void PlayerEntity::draw(grg::Graphic& graphic)
{
	graphic.draw(mSprite, sf::RenderStates::Default);
}

bool PlayerEntity::moveBackward()
{
	move(0, 1);
	return true;
}

bool PlayerEntity::moveForward()
{
	move(0, -1);
	return true;
}

bool PlayerEntity::moveLeft()
{
	move(-1, 0);
	return true;
}

bool PlayerEntity::moveRight()
{
	move(1, 0);
	return true;
}

bool PlayerEntity::use()
{
	return true;
}

bool PlayerEntity::jump()
{
	return true;
}

void PlayerEntity::close()
{

}

void PlayerEntity::move(int xDirection, int yDirection)
{
	//mCurrentPosition.x += (playerVO->getSpeed() * xDirection);
	//mCurrentPosition.y += (playerVO->getSpeed() * yDirection);
}

} //namespace Game
