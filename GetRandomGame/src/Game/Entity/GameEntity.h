/*
 * GameObject.h
 *
 *  Created on: 2013-01-30
 *      Author: bebleu
 */

#ifndef GAMEOBJECT_H_
#define GAMEOBJECT_H_

#include "Game/Action/Listener/ICharacterActionListener.h"
#include "Game/World/WorldCoord.h"
#include "GRGEngine/Graphic/Sprite.h"
#include <SFML/System/Vector2.hpp>

namespace grg
{
	class Graphic;
}

namespace Game{


class GameEntity: public ICharacterActionListener
{
public:
	GameEntity();

	GameEntity(int id);
	virtual ~GameEntity();

	virtual void update(const int & frameDelay) =0;
	virtual void draw(grg::Graphic& graphic) =0;
	virtual bool init() =0;
	virtual void close() =0;

	const WorldCoord& getCurrentPosition() const;
	void setCurrentPosition(const WorldCoord& currentPosition);

	const WorldCoord& getLastPosition() const;

protected:
	int mId;
	grg::Sprite mSprite;
	WorldCoord mCurrentPosition;
	WorldCoord mLastPosition;
};

}
#endif /* GAMEOBJECT_H_ */
