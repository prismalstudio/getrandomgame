/*
 * GameObject.cpp
 *
 *  Created on: 2013-01-30
 *      Author: bebleu
 */

#include "GameEntity.h"
#include "GRGEngine/Graphic/Sprite.h"

namespace Game
{

GameEntity::GameEntity() :
				mId(-1)
{
}

GameEntity::GameEntity(int id):
		mId(id)
{
}

GameEntity::~GameEntity()
{
// TODO Auto-generated destructor stub
}

const WorldCoord& GameEntity::getCurrentPosition() const
{
	return mCurrentPosition;
}

void GameEntity::setCurrentPosition(const WorldCoord& currentPosition)
{
	mLastPosition = mCurrentPosition;
	mCurrentPosition = currentPosition;
}

const WorldCoord& GameEntity::getLastPosition() const
{
	return mLastPosition;
}

} //namespace Game
