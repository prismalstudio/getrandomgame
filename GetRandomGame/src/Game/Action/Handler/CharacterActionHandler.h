/*
 * InputManager.h
 *
 *  Created on: 2013-09-17
 *      Author: Bidou
 */

#ifndef CHARACTER_ACTION_HANDLER_H_
#define CHARACTER_ACTION_HANDLER_H_

#include "GRGEngine/Event/ListenerInterface/IKeyboardListener.h"
#include "GRGEngine/Event/ListenerInterface/IMouseListener.h"
#include "GRGEngine/Event/ListenerInterface/IMessageListener.h"
#include <Game/Action/Listener/ICharacterActionListener.h>
#include <map>

namespace grg
{
	class Engine;
	class Event;
}
namespace sf
{
	class Keyboard;
}

namespace Game
{
	enum CharacterAction
	{
		MOVE_FORWARD = 0,
		MOVE_BACKWARD,
		MOVE_TO_THE_LEFT,
		MOVE_TO_THE_RIGHT,
		JUMP,
		USE,
		NOTHING
	};

	class CharacterActionHandler:
			public grg::IMouseListener,
			public grg::IKeyboardListener,
			public grg::IMessageListener
	{
		public:

			CharacterActionHandler(ICharacterActionListener* character);
			~CharacterActionHandler();

			virtual bool mouseButtonPressed(const sf::Event& event);
			virtual bool mouseButtonReleased(const sf::Event& event);
			virtual bool mouseMoved(const sf::Event& event);
			virtual bool mouseWheelMoved(const sf::Event& event);

			virtual bool keyPressed(const sf::Event& event);
			virtual bool keyReleased(const sf::Event& event);
			virtual bool textEntered(const sf::Event& event);

			virtual bool handleAction(Game::CharacterAction& action);
			virtual bool handleMessage(const grg::Event& event);

		private:
			ICharacterActionListener * mCharacter;
//			std::map< sf::Keyboard::Key , CharacterAction > mKeyToActionMap;

	};

} // namespace Game

#endif /* CHARACTER_ACTION_HANDLER_H_ */
