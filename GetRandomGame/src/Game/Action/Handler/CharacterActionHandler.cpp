/*
 * InputManager.cpp
 *
 *  Created on: 2013-09-17
 *      Author: Bidou
 */

#include "CharacterActionHandler.h"
#include "GRGEngine/Event/Event.h"
#include "GRGEngine/Engine.h"
#include "Util/Resource/TResourceManager.h"
#include "Util/Resource/LuaResource.h"

#include <SFML/Window/Event.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <iostream>

using Game::CharacterAction;

namespace Game
{
	CharacterActionHandler::CharacterActionHandler(ICharacterActionListener* character):mCharacter(character)
	{
//		mKeyToActionMap[sf::Keyboard::W]		= Game::MOVE_FORWARD;
//		mKeyToActionMap[sf::Keyboard::A] 		= Game::MOVE_TO_THE_LEFT;
//		mKeyToActionMap[sf::Keyboard::S] 		= Game::MOVE_BACKWARD;
//		mKeyToActionMap[sf::Keyboard::D] 		= Game::MOVE_TO_THE_RIGHT;
//		mKeyToActionMap[sf::Keyboard::Space] 	= Game::JUMP;
//		mKeyToActionMap[sf::Keyboard::E] 		= Game::USE;

		Util::LuaResource controlConfig = *Util::TResourceManager<Util::LuaResource>::getInstance()->load("asset/configs/controls.lua");
		std::cout << "MOVE_FORWARD = " << controlConfig.getStringValue("MOVE_FORWARD") << std::endl;
		std::cout << "MOVE_BACKWARD = " << controlConfig.getStringValue("MOVE_BACKWARD") << std::endl;
		std::cout << "MOVE_TO_THE_LEFT = " << controlConfig.getStringValue("MOVE_TO_THE_LEFT") << std::endl;
		std::cout << "MOVE_TO_THE_RIGHT = " << controlConfig.getStringValue("MOVE_TO_THE_RIGHT") << std::endl;
		std::cout << "USE = " << controlConfig.getStringValue("USE") << std::endl;
		std::cout << "JUMP = " << controlConfig.getStringValue("JUMP") << std::endl;

	}
	CharacterActionHandler::~CharacterActionHandler()
	{
	}

	bool CharacterActionHandler::keyPressed(const sf::Event& event)
	{
//		mKeyToActionMap[sf::Keyboard::W] = Game::MOVE_FORWARD;

		// take the action related to the keyboard key.
//		std::map< sf::Keyboard::Key, CharacterAction>::iterator it = mKeyToActionMap.find(event.key.code);
//
//		// if there is an action for that key,
//		if (it != mKeyToActionMap.end()) {
//			return handleAction(it->second);
//		}

		//TODO: ** See commented code above **
		CharacterAction action = NOTHING;

		switch (event.key.code)
		{
			case sf::Keyboard::A:
				action = MOVE_TO_THE_LEFT;
				break;
			case sf::Keyboard::S:
				action = MOVE_BACKWARD;
				break;
			case sf::Keyboard::W:
				action = MOVE_FORWARD;
				break;
			case sf::Keyboard::D:
				action = MOVE_TO_THE_RIGHT;
				break;
			case sf::Keyboard::Space:
				action = JUMP;
				break;
			case sf::Keyboard::E:
				action = USE;
				break;
			default:
				action = NOTHING;
		}

		if(action != NOTHING)
		{
			return handleAction(action);
		}

		return false;
	}

	bool CharacterActionHandler::handleAction(CharacterAction& action)
	{
		bool actionHandled	= false;

		switch (action)
		{
			case MOVE_BACKWARD:
				actionHandled = mCharacter->moveBackward();
				break;
			case MOVE_FORWARD:
				actionHandled = mCharacter->moveForward();
				break;
			case MOVE_TO_THE_LEFT:
				actionHandled = mCharacter->moveLeft();
				break;
			case MOVE_TO_THE_RIGHT:
				actionHandled = mCharacter->moveRight();
				break;
			case USE:
				actionHandled = mCharacter->use();
				break;
			case JUMP:
				actionHandled = mCharacter->jump();
				break;
			default:
				actionHandled = false;
		}

		return actionHandled;
	}

	bool CharacterActionHandler::mouseButtonPressed(const sf::Event& event)
	{
		return false;
	}

	bool CharacterActionHandler::mouseButtonReleased(const sf::Event& event)
	{
		return false;
	}
	bool CharacterActionHandler::mouseMoved(const sf::Event& event)
	{
		return false;
	}
	bool CharacterActionHandler::mouseWheelMoved(const sf::Event& event)
	{
		return false;
	}

	bool CharacterActionHandler::keyReleased(const sf::Event& event)
	{
		return false;
	}
	bool CharacterActionHandler::textEntered(const sf::Event& event)
	{
		return false;
	}
	bool CharacterActionHandler::handleMessage(const grg::Event& event)
	{
		return false;
	}

}
