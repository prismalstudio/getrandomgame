/*
 * ICharacterActionListener.h
 *
 *  Created on: 2013-10-27
 *      Author: Bidou
 */

#ifndef ICHARACTERACTIONLISTENER_H_
#define ICHARACTERACTIONLISTENER_H_

namespace Game
{

class ICharacterActionListener
{
public :
	virtual ~ICharacterActionListener(){}

	virtual bool moveLeft(void) 	= 0;
	virtual bool moveRight(void)	= 0;
	virtual bool moveForward(void)	= 0;
	virtual bool moveBackward(void)	= 0;
	virtual bool jump(void)			= 0;
	virtual bool use(void)			= 0;
};


}

#endif /* ICHARACTERACTIONLISTENER_H_ */
