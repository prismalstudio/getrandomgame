/**
 * @file		GameMain.cpp
 *
 * @date 		2012-07-10
 * @author		E. Bergeron
 * @copyright 	Prismal Studio 2008 www.prismalstudio.com
 * */

#include "Game/AppState/TestAppState.h"
#include "Game/AppState/MainMenuAppState.h"
#include "Game/AppState/GameTestAppState.h"
#include "Game/AppState/CharacterActionsState.h"

#include "GRGEngine/Engine.h"

#include <SFML/Graphics.hpp>

#include <GL/gl.h>

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

const int SCREEN_WIDTH = 1200;
const int SCREEN_HEIGHT = 750;
const unsigned int WINDOW_STYLE = sf::Style::Default;

void debugOutput(void);

int main()
{

	using grg::Engine;

	cout << "Launcher::main START" << endl;

	debugOutput();

	// set the flags for textual representation of boolean "true"
	cout << std::boolalpha;

	cout << "Creating the Engine..." << endl;
	// Get a pointer to the engine
	// which serves as a app state machine
	Engine * eng = Engine::getInstance();
	if (!eng)
	{
		cerr << "Error creating the Engine..." << endl;
		return -1;
	}
	cout << "Engine OK" << endl; // engine OK

	cout << "Adding the AppStates..." << endl;
	// push the Environment(s) (app state) we want
	// WARNING: Do not change the order without adjusting AppStateId.h,
	// to be changed with the separation of the Engine and the AppSateMachine.
	eng->PushAppState(new (std::nothrow) Game::MainMenuAppState(eng));
	cout << "MainMenuAppState OK" << endl;
	eng->PushAppState(new (std::nothrow) Game::GameTestAppState(eng));
	cout << "GameTestAppState OK" << endl;
	eng->PushAppState(new (std::nothrow) Game::TestAppState(eng));
	cout << "TestAppState OK" << endl;
	eng->PushAppState(new (std::nothrow) Game::CharacterActionsState(eng));
	cout << "TestInputManagerState OK" << endl;

	cout << "Starting the game..." << endl;
	// init it with custom screen size
	if (eng->init(SCREEN_WIDTH, SCREEN_HEIGHT, "Tile Game test", WINDOW_STYLE))
	{
		// then start this sh!t up
		eng->start();
	}
	else
	{
		cerr << "Error in Engine INIT..." << endl;
		return -1;
	}

	cout << "Launcher::main END" << endl;

	return 0;
}

void debugOutput(void)
{
	cout << "Max texture size: " << GL_MAX_TEXTURE_SIZE << endl;
}

