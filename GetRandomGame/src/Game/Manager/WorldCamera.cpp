/** 
 *  @file		WorldCamera.cpp
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-08-19
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#include "WorldCamera.h"

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Window/Event.hpp>
#include <iostream>

using std::cout;
using std::endl;
using std::cerr;
namespace Game
{

WorldCamera::WorldCamera() :
				mZoomFactor(1.0f),
				mWindow(NULL)
{
	// TODO Auto-generated constructor stub

}
WorldCamera::WorldCamera(const WorldCoord& coord,
		const sf::RenderTarget * window) :
				mTargetCoord(coord),
				mWorldView(window->getView()),
				mBackupView(window->getView()),
				mZoomFactor(1.0f),
				mWindow(window)
{
}

WorldCamera::WorldCamera(const WorldCoord& coord,
		const sf::RenderTarget * window, const sf::View& worldView) :
				mTargetCoord(coord),
				mWorldView(worldView),
				mBackupView(worldView),
				mZoomFactor(1.0f),
				mWindow(window)
{
}

WorldCamera::~WorldCamera()
{
	// TODO Auto-generated destructor stub
}

const Game::WorldCoord& WorldCamera::getTargetCoord() const
{
	return mTargetCoord;
}

void WorldCamera::setTargetCoord(const Game::WorldCoord& targetCoord)
{
	mTargetCoord = targetCoord;
}

void WorldCamera::move(int x, int y)
{
	mTargetCoord.adjustOffset(x, y);
	//mWorldView.move(x, -y);
}

Game::WorldCoord WorldCamera::screenPosToWorldCoord(
		const sf::Vector2i& pos) const
{

	sf::Vector2f convertedCoord = mWindow->mapPixelToCoords(pos, mWorldView);
	int xOffset = convertedCoord.x - mWorldView.getCenter().x;
	int yOffset = -(convertedCoord.y - mWorldView.getCenter().y);
	return mTargetCoord.getAdjustedCoordByOffset(sf::Vector2i(xOffset, yOffset));
}

/**
 * \brief Resize the view rectangle relatively to its current size
 * and zoom factor.
 *
 * Resizing the view simulates a zoom, as the zone displayed on
 * screen grows or shrinks.
 * \a factor is a multiplier:
 * \li 1 keeps the size unchanged
 * \li > 1 makes the view bigger (objects appear smaller)
 * \li < 1 makes the view smaller (objects appear bigger)
 *
 * \param factor Zoom factor to apply
 */
void WorldCamera::zoom(float factor)
{
	mZoomFactor += factor;
	mWorldView = mBackupView;
	mWorldView.zoom(mZoomFactor);
}

/**
 * \brief Resize the view rectangle relatively to its current size
 *
 * Resizing the view simulates a zoom, as the zone displayed on
 * screen grows or shrinks.
 * \a factor is a multiplier:
 * \li 1 keeps the size unchanged
 * \li > 1 makes the view bigger (objects appear smaller)
 * \li < 1 makes the view smaller (objects appear bigger)
 *
 * \param factor Zoom factor to apply
 */
void WorldCamera::setZoomFactor(float factor)
{
	mZoomFactor = factor;
	mWorldView = mBackupView;
	mWorldView.zoom(mZoomFactor);
}

float WorldCamera::getZoomFactor() const
{
	return mZoomFactor;
}
//const Game::WorldCoord& WorldCamera::getWorldCoordAt(float x, float y) const {
//	return Game::WorldCoord(mTargetCoord).adjustOffset(x -, y)
//}

const sf::View& WorldCamera::getWorldView() const
{
	return mWorldView;
}

void WorldCamera::setWorldView(const sf::View& worldView)
{
	mWorldView = worldView;
	mBackupView = worldView;
}

bool WorldCamera::handleMessage(const grg::Event& event)
{

	return false;
}

bool WorldCamera::closed(const sf::Event& event)
{
	return false;
}

bool WorldCamera::resized(const sf::Event& event)
{
	// FIXME: remove windows event handling from WorldCamera
	cout << "WorldCamera::resized to: (" << event.size.width << ", "
			<< event.size.height << ")" << endl;
	sf::Vector2f newSize(event.size.width, event.size.height);
	setWorldView(sf::View(sf::Vector2f(newSize.x / 2, newSize.y / 2), newSize));
	return false;
}

bool WorldCamera::gainedFocus(const sf::Event& event)
{
	//cout << "WorldCamera::gainedFocus" << endl;
	return false;
}

bool WorldCamera::lostFocus(const sf::Event& event)
{
	//cout << "WorldCamera::lostFocus" << endl;
	return false;
}

bool WorldCamera::mouseEntered(const sf::Event& event)
{
	//cout << "WorldCamera::mouseEntered" << endl;
	return false;
}

bool WorldCamera::mouseLeft(const sf::Event& event)
{
	//cout << "WorldCamera::mouseLeft" << endl;
	return false;
}

} /* namespace Game */
