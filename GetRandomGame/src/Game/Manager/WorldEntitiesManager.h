///*
// * WorldEntitiesManager.h
// *
// *  Created on: 2013-02-12
// *      Author: bebleu
// *
// *
// *  Handle EntitiesManager vs World
// */
//
//#ifndef WORLDENTITIESMANAGER_H_
//#define WORLDENTITIESMANAGER_H_
//
//#include "GameWorldManager.h"
//
//#include "EntityManager.h"
//
//namespace Game
//{
//class Tile;
//class Camera;
//
//class WorldEntitiesManager
//{
//	public:
//		WorldEntitiesManager();
//		virtual ~WorldEntitiesManager();
//
//		void initialize();
//		void initialize(GameWorldManager * worldManager,
//				EntityManager * entityManager);
//
//		void draw(int x = 0, int y = 0);
//		void update(const int &frameDelay);
//
//		EntityManager * getEntityManager() const;
//
//		void setEntityManager(const EntityManager*& entityManager);
//
//		//GameWorldManager* getWorldManager() const;
//
//		//void setWorldManager(const GameWorldManager*& worldManager);
//
//	private:
//		void internalInitialize();
//		void SetHoverTile(Tile *tile);
//
//		Tile * mHoverTile;
//		GameWorldManager * mWorldManager;
//		EntityManager * mEntityManager;
//
//		//TileEngine::Camera * mCam;
//
//};
//} //namespace Game
//#endif /* WORLDENTITIESMANAGER_H_ */
