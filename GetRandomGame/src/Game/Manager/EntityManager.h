/*
 * EntityManager.h
 *
 *  Created on: 2013-02-12
 *      Author: bebleu
 *
 *  Handle all entities (Player, enemies, etc)
 *
 */

#ifndef ENTITYMANAGER_H_
#define ENTITYMANAGER_H_

#include "Game/Entity/PlayerEntity.h"
#include<vector>
#include <stdint.h>

namespace grg{
	class Graphic;
}

namespace Game{

class GameEntity;

class EntityManager {
public:
	EntityManager();
	virtual ~EntityManager();

	void initialize();
	void draw(grg::Graphic& graphic);
	void update(const int &frameDelay);

	const std::vector<GameEntity*>& getEntities() const;

//	void addPlayer(Game::PlayerEntity * player);
//	PlayerEntity * getPlayer() const;

	void addEntity(GameEntity * entity);
	bool removeEntity(GameEntity * entity);

private:
	std::vector<GameEntity*> mEntities;
	PlayerEntity * mPlayer;

};
}//namespace Game
#endif /* ENTITYMANAGER_H_ */
