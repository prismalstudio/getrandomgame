/** 
 *  @file		MapManager.cpp
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-09-05
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#include "MapManager.h"

#include "Game/World/AreaMap.h"
#include "Game/World/WorldMap.h"

#include <math.h> // ceil
namespace Game {

MapManager::MapManager(Game::WorldMap* worldMap, const Game::WorldCoord& target,
		const sf::Vector2u& minArea, const sf::Vector2u& maxArea) :
				mWorldMap(worldMap),
				mCoord(target),
				mMinArea(minArea),
				mMaxArea(maxArea) {
	// TODO Auto-generated constructor stub

}

MapManager::~MapManager() {
	// TODO Auto-generated destructor stub
}

bool MapManager::init() {

	int xMod = ceil((double) mMinArea.x / 2);
	int yMod = ceil((double) mMinArea.y / 2);
	WorldCoord bottomLeft = mCoord;
	bottomLeft.adjustArea(-xMod, -yMod);

	//mLoadedAreaList.push_back()

	return true;
}

void MapManager::update(const int& deltaTime) {
}

void MapManager::loadRightCol() {
}

void MapManager::loadTopRow() {
}

void MapManager::loadLeftCol() {
}

void MapManager::loadBottomRow() {
}

void MapManager::dumpRightCol() {
}

void MapManager::dumpTopRow() {
}

void MapManager::dumpLeftCol() {
}

void MapManager::dumpBottomRow() {
}

} /* namespace Game */
