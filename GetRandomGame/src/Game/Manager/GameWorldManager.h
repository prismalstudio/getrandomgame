/** 
 *  @file		GameWorldManager.h
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-08-01
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#ifndef GAMEWORLDMANAGER_H_
#define GAMEWORLDMANAGER_H_

#include "GRGEngine/Interface/Drawable.h"
#include "GRGEngine/Event/ListenerInterface/IKeyboardListener.h"
#include "GRGEngine/Event/ListenerInterface/IMouseListener.h"

#include "WorldCamera.h"

#include "GRGEngine/GUI/GLabel.h"

namespace Game
{
class WorldMap;
/*
 * TODO make drawable again when removing debug output, to make draw a const function
 */
class GameWorldManager: /*public grg::Drawable,*/
public grg::IKeyboardListener, public grg::IMouseListener
{
	public:
		GameWorldManager();
		GameWorldManager(Game::WorldMap* worldMap,
				const Game::WorldCamera& worldCamera);
		virtual ~GameWorldManager();

		virtual void update(const int& deltaTime);
		virtual void draw(grg::Graphic& graphic);

		Game::WorldCamera* getCamera();
		void setCamera(const Game::WorldCamera& cam);
		Game::WorldMap* getWorldMap() const;

		virtual bool mouseButtonPressed(const sf::Event& event);
		virtual bool mouseButtonReleased(const sf::Event& event);
		virtual bool mouseMoved(const sf::Event& event);
		virtual bool mouseWheelMoved(const sf::Event& event);

		virtual bool keyPressed(const sf::Event& event);
		virtual bool keyReleased(const sf::Event& event);
		virtual bool textEntered(const sf::Event& event);

	private:
		Game::WorldMap* mWorldMap;
		Game::WorldCamera mCamera;

		sf::Vector2i mMousePos;
		sf::Vector2i mRelMousePos;
		sf::RectangleShape mMouseRect;

		// test var
		grg::GLabel mCoordLabel; // cam coord debug
		grg::GLabel mTestLabel; // test text output
		sf::RectangleShape mTestRect; // output debug
};

} /* namespace Game */
#endif /* GAMEWORLDMANAGER_H_ */
