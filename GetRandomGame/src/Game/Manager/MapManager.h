/** 
 *  @file		MapManager.h
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-09-05
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#ifndef MAPMANAGER_H_
#define MAPMANAGER_H_

#include "Game/World/WorldCoord.h"

#include <deque>

namespace Game {
class WorldMap;
class AreaMap;
/*
 *
 */
class MapManager {
public:
	MapManager(Game::WorldMap* worldMap, const Game::WorldCoord& target,
			const sf::Vector2u& minArea, const sf::Vector2u& maxArea);
	virtual ~MapManager();

	bool init();
	void update(const int& deltaTime);

	void loadRightCol();
	void loadTopRow();
	void loadLeftCol();
	void loadBottomRow();

	void dumpRightCol();
	void dumpTopRow();
	void dumpLeftCol();
	void dumpBottomRow();

private:
	Game::WorldMap* mWorldMap;
	Game::WorldCoord mCoord;
	sf::Vector2u mMinArea;
	sf::Vector2u mMaxArea;
	std::deque<std::deque<Game::AreaMap*> > mLoadedAreaList;
};

} /* namespace Game */
#endif /* MAPMANAGER_H_ */
