/*
 * EntityManager.cpp
 *
 *  Created on: 2013-02-12
 *      Author: bebleu
 */

#include "EntityManager.h"
namespace Game{
EntityManager::EntityManager() :
		mPlayer(NULL)
	{


}

EntityManager::~EntityManager() {
	// TODO Auto-generated destructor stub
}

const std::vector<GameEntity*>& EntityManager::getEntities() const {
	return mEntities;
}

void EntityManager::addEntity(GameEntity* entity) {

	//TODO : Put in function
	//Validate if entity exist
	std::vector<GameEntity *>::size_type i;
	for (i = 0; i < mEntities.size(); i++){
		if(mEntities.at(i) == entity){
			return;
		}
	}

	mEntities.insert(mEntities.end(),entity);
}

void EntityManager::initialize() {
}


void EntityManager::update(const int &frameDelay) {
	for (unsigned int i = 0; i < mEntities.size(); i++){
		if(mEntities.at(i))
		{
			mEntities.at(i)->update(frameDelay);
		}
	}
}

void EntityManager::draw(grg::Graphic& graphic) {
	for (unsigned int i = 0; i < mEntities.size(); i++){
		if(mEntities.at(i))
		{
			mEntities.at(i)->draw(graphic);
		}
	}
}

//void EntityManager::addPlayer(PlayerEntity* player) {
//	mPlayer = player;
//}
//
//PlayerEntity* EntityManager::getPlayer() const {
//	return mPlayer;
//}

bool EntityManager::removeEntity(GameEntity* entity) {

	//TODO : Put in function
	//Validate if entity exist
	std::vector<GameEntity *>::size_type i;
	int j=-1;
	for (i = 0; i < mEntities.size(); i++){
		j++;
		if(mEntities.at(i) == entity){
			break;
		}
	}


	if(j>-1){
		mEntities.erase(mEntities.begin() + j);
		return true;
	}

	return false;
}
}//namespace Game
