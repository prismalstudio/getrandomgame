/** 
 *  @file		WorldCamera.h
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-08-19
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#ifndef WORLDCAMERA_H_
#define WORLDCAMERA_H_

#include "GRGEngine/Event/ListenerInterface/IWindowListener.h"
#include "GRGEngine/Event/ListenerInterface/IMessageListener.h"

#include "Game/World/WorldCoord.h"

#include <SFML/Graphics/View.hpp>

namespace sf
{
class RenderTarget;
}

namespace Game
{

/*
 *
 */
class WorldCamera: public grg::IWindowListener, public grg::IMessageListener
{
	public:
		WorldCamera();
		WorldCamera(const WorldCoord& coord, const sf::RenderTarget * window);
		WorldCamera(const WorldCoord& coord, const sf::RenderTarget * window,
				const sf::View& worldView);
		virtual ~WorldCamera();

		const Game::WorldCoord& getTargetCoord() const;
		void setTargetCoord(const Game::WorldCoord& targetCoord);

		Game::WorldCoord screenPosToWorldCoord(const sf::Vector2i& pos) const;

		void move(int x, int y);
		void zoom(float factor);
		void setZoomFactor(float factor);
		float getZoomFactor() const;

		const sf::View& getWorldView() const;
		void setWorldView(const sf::View& worldView);

		//const Game::WorldCoord& getWorldCoordAt(float x, float y) const;

		virtual bool handleMessage(const grg::Event& event);

		virtual bool closed(const sf::Event& event);
		virtual bool resized(const sf::Event& event);
		virtual bool gainedFocus(const sf::Event& event);
		virtual bool lostFocus(const sf::Event& event);

		virtual bool mouseEntered(const sf::Event& event);
		virtual bool mouseLeft(const sf::Event& event);

	private:
		Game::WorldCoord mTargetCoord;
		sf::View mWorldView;
		sf::View mBackupView; ///< serves to unzoom
		float mZoomFactor;
		const sf::RenderTarget * mWindow;

};

} /* namespace Game */
#endif /* WORLDCAMERA_H_ */
