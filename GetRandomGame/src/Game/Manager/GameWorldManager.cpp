/** 
 *  @file		GameWorldManager.cpp
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-08-01
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#include "GameWorldManager.h"

#include "Game/World/Tile.h"
#include "Game/World/WorldMap.h"

#include "GRGEngine/Graphic/Graphic.h"

#include "Util/StringExt.h"

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/Event.hpp>
#include <iostream>

using std::cout;
using std::endl;
using std::cerr;
namespace Game
{

GameWorldManager::GameWorldManager() :
				mWorldMap(NULL),
				mCamera()
{

}

GameWorldManager::GameWorldManager(Game::WorldMap* worldMap,
		const Game::WorldCamera& worldCamera) :
				mWorldMap(worldMap),
				mCamera(worldCamera),
				mCoordLabel(worldCamera.getTargetCoord().toString(),
						"asset/font/caslonBold.ttf")
{
	cout << "New GameWorldManager" << endl;

	// test coord label
	mCoordLabel.setOffset(10, 5);
	mCoordLabel.getTextOb()->setColor(sf::Color::White);

	// test text output
	mTestLabel = mCoordLabel;
	mTestLabel.getTextOb()->setScale(0.6f, 0.6f);

	// test output rect
	mTestRect.setFillColor(sf::Color::Transparent);
	mTestRect.setOutlineColor(sf::Color::Red);
	mTestRect.setOutlineThickness(1);

	mMouseRect.setFillColor(sf::Color::Transparent);
	mMouseRect.setOutlineThickness(1);
	mMouseRect.setOutlineColor(sf::Color::Green);
	mMouseRect.setSize(
			sf::Vector2f(WorldCoord::getMaxTileSizeX(),
					WorldCoord::getMaxTileSizeY()));

	cout << "New GameWorldManager::END" << endl;
}

GameWorldManager::~GameWorldManager()
{
	// TODO Auto-generated destructor stub
}

void GameWorldManager::update(const int& deltaTime)
{

	WorldCoord mouseCoord = mCamera.screenPosToWorldCoord(mMousePos);

	sf::Vector2i tileSize = WorldCoord::getTileSize();
	sf::Vector2f screenSize = mCamera.getWorldView().getSize();

	// get the screen corners in world coordinate
	WorldCoord topRight = mCamera.getTargetCoord();
	topRight.adjustOffset(screenSize.x / 2, screenSize.y / 2);

	WorldCoord currentCoord;
	WorldCoord currentRow = topRight;

	sf::Vector2i curPos;
	sf::Vector2i tilePos = sf::Vector2i(screenSize.x - topRight.getOffsetX(),
			topRight.getOffsetY() - 40);

	/**
	 * SET the position
	 */
	// takes each rows
	while (tilePos.y <= (int) (screenSize.y + tileSize.y))
	{
		// then update each tile of the current row
		curPos = tilePos;
		currentCoord = currentRow;
		while (curPos.x > -(int) tileSize.x)
		{

			//DEBUG
			if (currentCoord.compareTile(mouseCoord))
			{
				mMouseRect.setPosition(curPos.x, curPos.y);
			}

			Tile* curTile = mWorldMap->getTile(currentCoord);
			curTile->setPosition(curPos.x, curPos.y);
			curTile->update(deltaTime);

			// lastly, adjust the current position
			curPos.x -= tileSize.x;
			currentCoord.adjustTileX(-1);
		}
		tilePos.y += tileSize.y;
		currentRow.adjustTileY(-1);
	}

}

void GameWorldManager::draw(grg::Graphic& graphic)
{

	sf::Vector2i tileSize = WorldCoord::getTileSize();
	//sf::Vector2u screenSize = graphic.getCurrentRenderTarget()->getSize();
	sf::Vector2f screenSize = mCamera.getWorldView().getSize();

	// get the screen corners in world coordinate
	WorldCoord topRight = mCamera.getTargetCoord();
	topRight.adjustOffset(screenSize.x / 2, screenSize.y / 2);

	// prepare the view
	sf::View viewBackup = graphic.getCurrentRenderTarget()->getView();
	graphic.getCurrentRenderTarget()->setView(mCamera.getWorldView());
	/**
	 * DRAW the tiles
	 */
	WorldCoord currentCoord;
	WorldCoord currentRow = topRight;

	sf::Vector2i curPos;
	sf::Vector2i tilePos = sf::Vector2i(screenSize.x - topRight.getOffsetX(),
			topRight.getOffsetY() - 40);

	// takes each rows
	while (tilePos.y <= (int) (screenSize.y + tileSize.y))
	{
		// then draw each tile of the current row
		curPos = tilePos;
		currentCoord = currentRow;
		while (curPos.x > -(int) tileSize.x)
		{
			Tile* curTile = mWorldMap->getTile(currentCoord);
			curTile->draw(graphic);
			// lastly, adjust the current position
			curPos.x -= tileSize.x;
			currentCoord.adjustTileX(-1);
		}
		tilePos.y += tileSize.y;
		currentRow.adjustTileY(-1);
	}

	graphic.draw(mMouseRect);

	// draw the view contour
	//	mTestRect.setSize(mCamera.getWorldView().getSize());
	//	mTestRect.setOutlineColor(sf::Color::Green);
	//	mTestRect.setPosition(0, 0);
	//	graphic.draw(mTestRect);

	// draw a test crosshair
	mTestRect.setFillColor(sf::Color::Cyan);
	mTestRect.setOutlineThickness(0);
	mTestRect.setSize(sf::Vector2f(2, 30));
	mTestRect.setPosition(screenSize.x / 2 - mTestRect.getSize().x / 2,
			screenSize.y / 2 - mTestRect.getSize().y / 2);
	graphic.draw(mTestRect);

	mTestRect.setSize(sf::Vector2f(30, 2));
	mTestRect.setPosition(screenSize.x / 2 - mTestRect.getSize().x / 2,
			screenSize.y / 2 - mTestRect.getSize().y / 2);
	graphic.draw(mTestRect);

	// draw the cam world coordinate
	mCoordLabel.getTextOb()->setString(mCamera.getTargetCoord().toString());
	mCoordLabel.setOffset(screenSize.x / 2 + 10, screenSize.y / 2 + 5);
	mCoordLabel.draw(graphic);

	// draw the top Right world coord
	mCoordLabel.getTextOb()->setString("Top Right: " + topRight.toString());
	mCoordLabel.setOffset(screenSize.x - 550, 5);
	mCoordLabel.draw(graphic);

//	mCoordLabel.getTextOb()->setString("Mouse Coord: " + mouseCoord.toString());
//	mCoordLabel.setOffset(10, 10);
//	mCoordLabel.draw(graphic);

// reset the view
	graphic.getCurrentRenderTarget()->setView(viewBackup);
}

Game::WorldCamera* GameWorldManager::getCamera()
{
	return &mCamera;
}

void GameWorldManager::setCamera(const Game::WorldCamera& cam)
{
	mCamera = cam;
}

Game::WorldMap * GameWorldManager::getWorldMap() const
{
	return mWorldMap;
}

bool GameWorldManager::mouseButtonPressed(const sf::Event& event)
{
	return false;
}

bool GameWorldManager::mouseButtonReleased(const sf::Event& event)
{
	return false;
}

bool GameWorldManager::mouseMoved(const sf::Event& event)
{

	// FIXME related to card #97, relative mouse movement isn't provided by SFML.
	mRelMousePos.x = event.mouseMove.x - mMousePos.x;
	mRelMousePos.y = event.mouseMove.y - mMousePos.y;
	mMousePos.x = event.mouseMove.x;
	mMousePos.y = event.mouseMove.y;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl))
	{

//		cout << "LControl + Mouse Moved by: (" << mRelMousePos.x << ", "
//				<< mRelMousePos.y << ")" << endl;
//		cout << "View center pos: (" << mCamera.getWorldView().getCenter().x
//				<< ", " << mCamera.getWorldView().getCenter().y << ")" << endl;

		mCamera.move(-mRelMousePos.x, mRelMousePos.y);

	}
	else if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		WorldCoord mouseCoord = mCamera.screenPosToWorldCoord(mMousePos);
		mWorldMap->getTile(mouseCoord)->SetTileType(1);
	}
	return false;
}

bool GameWorldManager::mouseWheelMoved(const sf::Event& event)
{
//	cout << "mCamera zoom factor: " << mCamera.getZoomFactor() << endl;
////	sf::View view = mCamera.getWorldView();
////	float factor = ((float) event.mouseWheel.delta / 100);
	mCamera.zoom((float) event.mouseWheel.delta / 100);

	return false;
}

bool GameWorldManager::keyPressed(const sf::Event& event)
{
	bool handled = false;
	switch (event.key.code)
	{
	case sf::Keyboard::Num0:

		handled = true;
		break;
	case sf::Keyboard::Num1:

		handled = true;
		break;
	case sf::Keyboard::Num2:

		handled = true;
		break;
	default:
		break;
	}
	return handled;
}

bool GameWorldManager::keyReleased(const sf::Event& event)
{
	return false;
}

bool GameWorldManager::textEntered(const sf::Event& event)
{
	return false;
}

} /* namespace Game */
