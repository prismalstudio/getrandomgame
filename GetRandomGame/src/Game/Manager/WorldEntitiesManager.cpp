///*
// * WorldEntitiesManager.cpp
// *
// *  Created on: 2013-02-12
// *      Author: bebleu
// */
//
//#include "WorldEntitiesManager.h"
//
//#include "Game/World/Tile.h"
//#include "GRGEngine/Graphic/Graphic.h"
//
//#include <stdio.h>
//
//namespace Game{
//
//	WorldEntitiesManager::WorldEntitiesManager() {
//	mWorldManager = NULL;
//	mEntityManager = NULL;
//	mHoverTile = NULL;
//}
//
//WorldEntitiesManager::~WorldEntitiesManager() {
//}
//
//void WorldEntitiesManager::initialize() {
//	mWorldManager = new GameWorldManager();
//	mEntityManager = new EntityManager();
//
//	internalInitialize();
//}
//
//void WorldEntitiesManager::initialize(GameWorldManager* worldManager,
//		EntityManager* entityManager) {
//	mWorldManager = worldManager;
//	mEntityManager = entityManager;
//
//	internalInitialize();
//}
//
//void WorldEntitiesManager::draw(int x, int y) {
//	//if(mWorldManager!=NULL){
//	//	mWorldManager->draw(x,y);
//	//}
//	if(mEntityManager!=NULL){
//		mEntityManager->draw();
//	}
//}
//
//void WorldEntitiesManager::update(const int &frameDelay) {
//
//	if(mEntityManager)
//		mEntityManager->update(frameDelay);
//
//	//if(mWorldManager)
//	//	mWorldManager->update(frameDelay);
//
//	//TODO : REMOVE PLAYER TEST
//	//int myX = mEntityManager->getPlayer()->getCurrentPosition().x;
//	//int myY = mEntityManager->getPlayer()->getCurrentPosition().y;
//
//	//Why Screen_HEIght in Y ?
//	//Because screen start at 0,0 top_left
//	//Game is Bottom_left 0,0
//	// TODO : Bebleu, lis �a: c'est pas le jeu qui est bottom_left, c'est openGL par d�faut, nous pourrions
//	// y faire quelque chose, c'est � voir. Aussi, Global est a �vit�, pour les dimensions d'�cran, il est
//	// plus id�ale d'utiliser Graphic::getInstance()->GetHeight() puisque la dimension peut changer lors de
//	// l'activation du fullscreen.
//	//using TileEngine::Graphic; // pour �viter cette ligne, il est possible de mettre "using namespace TileEngine;" au d�but d'un fichier CPP SEULEMENT, pas un H.
//	//Tile * playerTile = mWorldManager->getTile(myX, myY + /*SCREEN_HEIGHT*/ Graphic::getInstance()->GetHeight());
//	//SetHoverTile(playerTile);
//}
//
//EntityManager * WorldEntitiesManager::getEntityManager() const{
//	return mEntityManager;
//}
//
//void WorldEntitiesManager::setEntityManager(
//		const EntityManager*& entityManager) {
//}
//
////WorldManager * WorldEntitiesManager::getWorldManager() const{
////	return mWorldManager;
////}
//
////void WorldEntitiesManager::setWorldManager(const WorldManager*& worldManager) {
////}
//
//void WorldEntitiesManager::internalInitialize() {
//	//mCam = TileEngine::Camera::getInstance();
//
//	mEntityManager->initialize();
//	//mWorldManager->initialize();
//}
//
////DEBUG FUNCTION
//void WorldEntitiesManager::SetHoverTile(Tile *tile) {
//	if (tile) {
//		if (mHoverTile) {
//			//mHoverTile->setActive(false);
//		}
//		mHoverTile = tile;
//		//mHoverTile->isActive();
//	}
//}
//}//namespace Game
