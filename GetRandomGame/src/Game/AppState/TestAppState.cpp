/** 
 *  @file		TestAppState.cpp
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-05-14
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#include "TestAppState.h"

#include "GRGEngine/Graphic/Graphic.h"
#include "GRGEngine/Engine.h"

#include <SFML/Window/Event.hpp>

namespace Game {

TestAppState::TestAppState(grg::Engine* eng) :
				grg::AppState(eng) {
}

TestAppState::~TestAppState() {
	// TODO Auto-generated destructor stub
}

bool TestAppState::Init() {
	//getEventManager()->registerInputListener(this);
	mTestSprite = grg::Sprite("asset/image/tileset_trans2.png");
	mTestSprite.setPosition(10, 100);
	mEngine->addKeyboardListener(this);
	return true;
}

void TestAppState::Reload() {
}

void TestAppState::Update(const int& frameTime) {
}

void TestAppState::Draw(grg::Graphic& graphic) {
	graphic.draw(mTestSprite);
}

void TestAppState::Close() {
}

bool TestAppState::keyPressed(const sf::Event& event) {
	return false;
}

bool TestAppState::keyReleased(const sf::Event& event) {
	switch (event.key.code) {
	case sf::Keyboard::Escape:
		//mEngine->NextAppState();
		mEngine->SetActiveAppState(0);
		return true;
		break;
	default:
		break;
	}
	return false;
}

bool TestAppState::textEntered(const sf::Event& event) {
	return false;
}

} /* namespace Game */
