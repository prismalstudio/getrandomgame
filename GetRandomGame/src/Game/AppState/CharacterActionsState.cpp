/*
 * CharacterActionsState.cpp
 *
 *  Created on: 2013-09-29
 *      Author: Bidou
 */

#include "CharacterActionsState.h"

#include "GRGEngine/Graphic/Graphic.h"
#include "GRGEngine/Event/Event.h"
#include "GRGEngine/Engine.h"

#include <SFML/Window/Event.hpp>

namespace Game
{
	CharacterActionsState::CharacterActionsState(grg::Engine* eng): grg::AppState(eng)
	{
		mPlayer	= new PlayerEntity();
		mCharacterActionHandler	= new CharacterActionHandler(mPlayer);
	}
	CharacterActionsState::~CharacterActionsState()
	{
	}

	bool CharacterActionsState::Init()
	{
		mEngine->addKeyboardListener(this);
		mEngine->addKeyboardListener(mCharacterActionHandler);
		mPlayer->init();

		return true;
	}

	void CharacterActionsState::Reload()
	{
	}

	void CharacterActionsState::Update(const int& frameTime)
	{
		mPlayer->update(frameTime);
	}

	void CharacterActionsState::Draw(grg::Graphic& graphic)
	{
		mPlayer->draw(graphic);
	}
	void CharacterActionsState::Close()
	{
	}

	bool CharacterActionsState::mouseButtonPressed(const sf::Event& event)
	{
		return false;
	}

	bool CharacterActionsState::mouseButtonReleased(const sf::Event& event)
	{
		return false;
	}

	bool CharacterActionsState::mouseMoved(const sf::Event& event)
	{
		return false;
	}

	bool CharacterActionsState::mouseWheelMoved(const sf::Event& event)
	{
		return false;
	}

	bool CharacterActionsState::keyPressed(const sf::Event& event)
	{
		return false;
	}

	bool CharacterActionsState::keyReleased(const sf::Event& event)
	{
		bool handled = false;
		switch (event.key.code)
		{
		case sf::Keyboard::Escape:
			mEngine->SetActiveAppState(0);
			handled = true;
			break;
		default:
			break;
		}

		return handled;
	}

	bool CharacterActionsState::textEntered(const sf::Event& event)
	{
		return false;
	}

	bool CharacterActionsState::handleMessage(const grg::Event& event)
	{
		return false;
	}

}



