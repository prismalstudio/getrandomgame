/*
 * MainMenuEnvironment.h
 *
 *  Created on: 2012-07-09
 *      Author: Emile
 */

#ifndef MAINMENUENVIRONMENT_H_
#define MAINMENUENVIRONMENT_H_

#include "GRGEngine/AppState.h"
#include "GRGEngine/Event/ListenerInterface/IKeyboardListener.h"
#include "GRGEngine/Event/ListenerInterface/IMouseListener.h"
#include "GRGEngine/Event/ListenerInterface/IMessageListener.h"


namespace grg {
class Graphic;
class Engine;
class GuiContainer;
class Sprite;
class Event;
}

namespace Game {

/*
 *
 */
class MainMenuAppState: public grg::AppState,
		public grg::IMouseListener,
		public grg::IKeyboardListener,
		public grg::IMessageListener {
public:
	MainMenuAppState(grg::Engine * eng);
	virtual ~MainMenuAppState() {

	}
	virtual bool Init(); // generic initialization
	virtual void Reload();
	virtual void Update(const int& frameDelay); // generic function, redefine by the subclass
	virtual void Draw(grg::Graphic& graphic); // generic function, redefine by the subclass
	virtual void Close(); // generic function, redefine by the subclass

	virtual bool mouseButtonPressed(const sf::Event& event);
	virtual bool mouseButtonReleased(const sf::Event& event);
	virtual bool mouseMoved(const sf::Event& event);
	virtual bool mouseWheelMoved(const sf::Event& event);

	virtual bool keyPressed(const sf::Event& event);
	virtual bool keyReleased(const sf::Event& event);
	virtual bool textEntered(const sf::Event& event);

	virtual bool handleMessage(const grg::Event& event);

private:
	grg::GuiContainer* mGuiContainer;

	/**
	 * Tests
	 */
	grg::Sprite* _spriteTest; // TODO create a GUI for images
};

} // namespace Game
#endif /* MAINMENUENVIRONMENT_H_ */
