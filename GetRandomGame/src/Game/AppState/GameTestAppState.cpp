/** 
 *  @file		GameTestAppState.cpp
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-07-31
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#include "GameTestAppState.h"

#include "Game/Manager/GameWorldManager.h"
#include "Game/World/WorldCoord.h"
#include "Game/World/WorldMap.h"

#include "GRGEngine/GUI/GuiContainer.h"
#include "GRGEngine/Engine.h"
#include "GRGEngine/Graphic/Graphic.h"

#include <SFML/Window/Event.hpp>

#include <iostream>

using std::cout;
using std::endl;
using std::cerr;

namespace Game
{

GameTestAppState::GameTestAppState(grg::Engine * eng) :
				grg::AppState(eng)
{
	mGameWorld = NULL;
	mGuiContainer = new (std::nothrow) grg::GuiContainer();

}

GameTestAppState::~GameTestAppState()
{
	delete mGameWorld;
}

bool GameTestAppState::Init()
{
	cout << "GameTestAppState::Init()" << endl;

	// set the max size of the world
	WorldCoord::setMaxBounds(257, 257, 65, 65, 17, 17, 40, 40);

	// create a new world
	WorldMap * worldMap = new (std::nothrow) WorldMap("WorldName",
			"data/world");
	cout << "GameTestAppState::Init(): WorldMap created..." << endl;

	// creating the GameWorldManager
	sf::RenderTarget * curTarget =
			mEngine->getGraphicOb()->getCurrentRenderTarget();
	mGameWorld = new (std::nothrow) Game::GameWorldManager(worldMap,
			WorldCamera(WorldCoord(25, 25, 20, 20, 10, 10, 0, 0), curTarget));
	if (!mGameWorld)
	{
		cerr << "GameTestAppState::Init()::ERROR loading GameWorldManager..."
				<< endl;
		return false;
	}
	mEngine->addWindowListener(mGameWorld->getCamera());
	mEngine->addMouseListener(mGameWorld);
	mEngine->addKeyboardListener(mGameWorld);
	cout << "GameTestAppState::Init(): GameWorldManager created..." << endl;

	mEngine->addKeyboardListener(this);
	//mEngine->addMessageListener(this)
	mEngine->addMouseListener(this);

	cout << "GameTestAppState::Init() END" << endl;
	return true;
}

void GameTestAppState::Reload()
{
}

void GameTestAppState::Update(const int& frameDelay)
{
	mGameWorld->update(frameDelay);
}

void GameTestAppState::Draw(grg::Graphic& graphic)
{
	mGameWorld->draw(graphic);
}

void GameTestAppState::Close()
{

	delete mGameWorld;
}

bool GameTestAppState::handleMessage(const grg::Event& event)
{
	return false;
}

bool GameTestAppState::mouseButtonPressed(const sf::Event& event)
{
	return false;
}

bool GameTestAppState::mouseButtonReleased(const sf::Event& event)
{
	return false;
}

bool GameTestAppState::mouseMoved(const sf::Event& event)
{
	return false;
}

bool GameTestAppState::mouseWheelMoved(const sf::Event& event)
{
	return false;
}

bool GameTestAppState::keyPressed(const sf::Event& event)
{
	return false;
}

bool GameTestAppState::keyReleased(const sf::Event& event)
{
	bool handled = false;
	switch (event.key.code)
	{
	case sf::Keyboard::Escape:
		mEngine->SetActiveAppState(0);
		handled = true;
		break;
	case sf::Keyboard::F:
		mEngine->getGraphicOb()->toggleFullscreen();
		handled = true;
		break;
	default:
		break;
	}

	return handled;
}

bool GameTestAppState::textEntered(const sf::Event& event)
{
	return false;
}

} /* namespace grg */
