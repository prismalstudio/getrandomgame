/*
 * MainMenuEnvironment.cpp
 *
 *  Created on: 2012-07-09
 *      Author: Emile
 */

#include "MainMenuAppState.h"

#include "AppStateId.h"

#include "GRGEngine/GUI/GTextArea.h"
#include "GRGEngine/GUI/GButton.h"
#include "GRGEngine/GUI/GuiContainer.h"
#include "GRGEngine/Graphic/Sprite.h"
//#include "Graphic/GraphicType.h"
#include "GRGEngine/Graphic/Graphic.h"
//#include "Graphic/GraphicData.h"
#include "GRGEngine/Resource/Texture.h"
#include "GRGEngine/Resource/Font.h"

#include "GRGEngine/Event/Event.h"
#include "GRGEngine/Engine.h"

#include "Util/StringExt.h"
#include "Util/Resource/TResourceManager.h"

#include <SFML/Graphics/Text.hpp>
#include <SFML/Window/Event.hpp>

#include <sstream>
#include <iostream>

using grg::EventManager;
using grg::GuiContainer;

using Util::toStr;

using std::cout;
using std::endl;
using std::cerr;

namespace Game
{
/**
 * TEST VAR
 */
sf::Text myText;
grg::GTextArea * _consoleTest;
/**
 * END TEST VAR
 ******************/

MainMenuAppState::MainMenuAppState(grg::Engine * eng) :
				grg::AppState(eng)
{
	_spriteTest = NULL;
	mGuiContainer = new (std::nothrow) GuiContainer();

}

/**
 * State methods
 * TODO find a nice way to know the screen size here
 */
bool MainMenuAppState::Init()
{

	// Main menu background
	_spriteTest = new (std::nothrow) grg::Sprite(
			"asset/image/splash_screen.png");

	// Debug size scaling
	_spriteTest->setScale(((float) 1200 / 1322), ((float) 1200 / 1322));

	// Debug Mouse coord sf::Text
	myText.setColor(sf::Color::Black);
	myText.setCharacterSize(20);
	myText.setFont(
			*Util::TResourceManager<grg::Font>::getInstance()->load(
					"asset/font/CaslonBold.ttf"));

	// create and adjust a GTextArea
	_consoleTest = new (std::nothrow) grg::GTextArea();
	// set that textbox default font, but each text can use its own font
	_consoleTest->SetFont("asset/font/CaslonBold.ttf");
	_consoleTest->setSize(600, 210);
	_consoleTest->setOrigin(3, 537);

	// Add some useful text informations
	_consoleTest->appendLine(
			"Welcome to the Alpha Version! This is a freaking long sentence to test auto text wrapping.");
	_consoleTest->appendLine("this is blue and italic...", sf::Color::Blue,
			sf::Text::Italic);
	_consoleTest->appendLine("third line with bigger letter and green color...",
			sf::Color::Green, sf::Text::Bold, 35);
	_consoleTest->appendLine(
			"another line with wrapping to be sure everything is working properly, so let's get that one really longgggggg!!");
	_consoleTest->appendLine("A line that we are not supposed to see...");

	// Push the text area to be drawn and held by the GUIcontainer
	mGuiContainer->pushGuiItem(_consoleTest);

	// Game Test Button
	grg::GButton * newButton = new (std::nothrow) grg::GButton(850, 537, 250,
			40, "Game Test", "asset/font/CaslonBold.ttf");
	grg::Event* tempEvent = new grg::Event(grg::Event::StateChanged);
	tempEvent->state.id = AppStateId::GameTest;
	newButton->setActivateEvent(tempEvent);
	mEngine->addMouseListener(newButton);
	mGuiContainer->pushGuiItem(newButton);

	// Test Button
	newButton = new grg::GButton(850, 587, 250, 40, "Test AppState",
			"asset/font/CaslonBold.ttf");
	tempEvent = new grg::Event(grg::Event::StateChanged);
	tempEvent->state.id = AppStateId::Test;
	newButton->setActivateEvent(tempEvent);
	mEngine->addMouseListener(newButton);
	mGuiContainer->pushGuiItem(newButton);

	// TestInputManagerState Button
	newButton = new grg::GButton(850, 637, 250, 40, "TestInputManagerState",
			"asset/font/CaslonBold.ttf");
	tempEvent = new grg::Event(grg::Event::StateChanged);
	tempEvent->state.id = AppStateId::CharacterAction;
	newButton->setActivateEvent(tempEvent);
	mEngine->addMouseListener(newButton);
	mGuiContainer->pushGuiItem(newButton);

	// Main Menu State registering for events
	mEngine->addKeyboardListener(this);
	mEngine->addMouseListener(this);
	mEngine->addMessageListener(grg::Event::StateChanged, this);

	return true;
}

void MainMenuAppState::Reload()
{
}

void MainMenuAppState::Update(const int& frameDelay)
{
}

/**
 * FIXME Eventually, make sure that this is const
 */
void MainMenuAppState::Draw(grg::Graphic& graphic)
{

	graphic.draw(*_spriteTest);

	mGuiContainer->draw(graphic);

	graphic.draw(myText);
}

/**
 * undo init
 */
void MainMenuAppState::Close()
{
	cout << "MainMenuAppState::Close()" << endl;
//	getEventManager()->unregisterInputListener(this);
//	getEventManager()->unregisterEventListener(0, this);
	mGuiContainer->clear();
	delete _spriteTest;
}

bool MainMenuAppState::mouseButtonPressed(const sf::Event& event)
{
	cout << "MainMenuAppState received MouseDown " << endl;
	return false;
}

bool MainMenuAppState::mouseButtonReleased(const sf::Event& event)
{
	return false;
}

bool MainMenuAppState::mouseMoved(const sf::Event& event)
{
	myText.setString(
			"(" + toStr(event.mouseMove.x) + ", " + toStr(event.mouseMove.y)
					+ ")");
	myText.setPosition(event.mouseMove.x + 20, event.mouseMove.y - 20);
	return false;
}

bool MainMenuAppState::mouseWheelMoved(const sf::Event& event)
{
	return false;
}

bool MainMenuAppState::keyPressed(const sf::Event& event)
{
	return false;
}

bool MainMenuAppState::keyReleased(const sf::Event& event)
{
	switch (event.key.code)
	{
	case sf::Keyboard::Escape:
		mEngine->Exit();
		return true;
		break;
	default:
		break;
	}
	return false;
}

bool MainMenuAppState::textEntered(const sf::Event& event)
{
	return false;
}

bool MainMenuAppState::handleMessage(const grg::Event& event)
{
	cout << "MainMenu received an Event" << endl;
	bool isEventHandled = false;
	if (event.getType() == grg::Event::StateChanged)
	{
		mEngine->SetActiveAppState(event.state.id);
		isEventHandled = true;
	}
	return isEventHandled;
}

} // namespace Game
