/*
 * CharacterActionsState.h
 *
 *  Created on: 2013-09-29
 *      Author: Bidou
 */

#ifndef CHARACTER_ACTIONS_STATE_H_
#define CHARACTER_ACTIONS_STATE_H_

#include "GRGEngine/AppState.h"
#include "GRGEngine/Event/ListenerInterface/IKeyboardListener.h"
#include "GRGEngine/Event/ListenerInterface/IMouseListener.h"
#include "GRGEngine/Event/ListenerInterface/IMessageListener.h"
#include "Game/Entity/PlayerEntity.h"

namespace grg
{
	class Graphic;
	class Engine;
	class GuiContainer;
	class Event;
	class GTextArea;
}

namespace Game
{

class CharacterActionsState: public grg::AppState,
		public grg::IMouseListener,
		public grg::IKeyboardListener,
		public grg::IMessageListener
{
	public:
	CharacterActionsState(grg::Engine * eng);
	virtual ~CharacterActionsState();

	virtual bool Init();
	virtual void Reload();
	virtual void Update(const int&);
	virtual void Draw(grg::Graphic& graphic);
	virtual void Close();

	virtual bool mouseButtonPressed(const sf::Event& event);
	virtual bool mouseButtonReleased(const sf::Event& event);
	virtual bool mouseMoved(const sf::Event& event);
	virtual bool mouseWheelMoved(const sf::Event& event);

	virtual bool keyPressed(const sf::Event& event);
	virtual bool keyReleased(const sf::Event& event);
	virtual bool textEntered(const sf::Event& event);

	virtual bool handleMessage(const grg::Event& event);

	private :

	CharacterActionHandler * mCharacterActionHandler;
	PlayerEntity * mPlayer;
};

}
#endif /* CHARACTER_ACTIONS_STATE_H_ */
