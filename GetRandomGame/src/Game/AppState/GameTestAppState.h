/** 
 *  @file		GameTestAppState.h
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-07-31
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#ifndef GAMETESTAPPSTATE_H_
#define GAMETESTAPPSTATE_H_

#include "GRGEngine/AppState.h"
#include "GRGEngine/Event/ListenerInterface/IKeyboardListener.h"
#include "GRGEngine/Event/ListenerInterface/IMouseListener.h"
#include "GRGEngine/Event/ListenerInterface/IMessageListener.h"


namespace grg {
class Graphic;
class Engine;
class GuiContainer;
class Event;
}

namespace Game {
class GameWorldManager;

/*
 *
 */
class GameTestAppState: public grg::AppState,
		public grg::IKeyboardListener,
		public grg::IMouseListener,
		public grg::IMessageListener {
public:
	GameTestAppState(grg::Engine * eng);
	virtual ~GameTestAppState();

	virtual bool Init(); // generic initialization
	virtual void Reload();
	virtual void Update(const int&); // generic function, redefine by the subclass
	virtual void Draw(grg::Graphic&); // generic function, redefine by the subclass
	virtual void Close(); // generic function, redefine by the subclass

	virtual bool handleMessage(const grg::Event& event);

	virtual bool mouseButtonPressed(const sf::Event& event);
	virtual bool mouseButtonReleased(const sf::Event& event);
	virtual bool mouseMoved(const sf::Event& event);
	virtual bool mouseWheelMoved(const sf::Event& event);

	virtual bool keyPressed(const sf::Event& event);
	virtual bool keyReleased(const sf::Event& event);
	virtual bool textEntered(const sf::Event& event);

private:
	Game::GameWorldManager * mGameWorld;
	grg::GuiContainer* mGuiContainer;

};

}
/* namespace grg */
#endif /* GAMETESTAPPSTATE_H_ */
