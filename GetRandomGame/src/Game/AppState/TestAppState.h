/** 
 *  @file		TestAppState.h
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-05-14
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#ifndef TESTAPPSTATE_H_
#define TESTAPPSTATE_H_

#include "GRGEngine/AppState.h"
#include "GRGEngine/Event/ListenerInterface/IKeyboardListener.h"

#include "GRGEngine/Graphic/Sprite.h"

namespace grg {
class Engine;
}

namespace Game {

/*
 *
 */
class TestAppState: public grg::AppState, public grg::IKeyboardListener {
public:
	TestAppState(grg::Engine * eng);
	virtual ~TestAppState();

	// Generic Environment Methods
	virtual bool Init(); // generic initialization
	virtual void Reload();
	virtual void Update(const int&); // generic function, redefine by the subclass
	virtual void Draw(grg::Graphic& graphic); // generic function, redefine by the subclass
	virtual void Close(); // generic function, redefine by the subclass

	virtual bool keyPressed(const sf::Event& event);
	virtual bool keyReleased(const sf::Event& event);
	virtual bool textEntered(const sf::Event& event);

private:
	grg::Sprite mTestSprite;
};

} /* namespace Game */
#endif /* TESTAPPSTATE_H_ */
