/**
 * @file		AppStateId.h
 *
 * @date 		2014-01-01
 * @author		E. Bergeron
 * @copyright 	Prismal Studio 2008 www.prismalstudio.com
 * */

#ifndef APPSTATEID_H_
#define APPSTATEID_H_

namespace Game
{
namespace AppStateId
{

enum
{
	MainMenu,
	GameTest,
	Test,
	CharacterAction,

	Count ///< Keep last, Application states count
};
}
}

#endif /* APPSTATEID_H_ */
