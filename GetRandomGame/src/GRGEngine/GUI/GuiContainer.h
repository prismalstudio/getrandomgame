/*
 * GMenuContainer.h
 *
 *  Created on: 2012-02-29
 *      Author: Emile
 *
 *      This is a generic menu container, you may redefine
 *      it in order to use the menu fonctionnality properly.
 *
 *      Add come GMenu to it.
 */

#ifndef GMENUCONTAINER_H_
#define GMENUCONTAINER_H_

//#include "GItem.h"

#include <vector>

namespace grg {
class Graphic;
class GItem;

/*
 *
 */
class GuiContainer {
public:
	GuiContainer();
	virtual ~GuiContainer();

	virtual void init() {
	}
	virtual void update(const int& frameDelay);
	virtual void draw(grg::Graphic& graphic) const;

	virtual void pushGuiItem(GItem*);
	virtual void clear();

protected:

	std::vector<GItem*> mItemList;

};

} /* namespace TileEngine */
#endif /* GMENUCONTAINER_H_ */
