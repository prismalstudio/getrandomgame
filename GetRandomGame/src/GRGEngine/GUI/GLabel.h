/*
 * GLabel.h
 *
 *  Created on: 2012-02-29
 *      Author: Emile
 *
 *      @date	2013-05-21	changed to SFML
 */

#ifndef GLABEL_H_
#define GLABEL_H_

#include "GItem.h"
//#include "GRGEngine/Resource/Font.h"

#include <SFML/Graphics/Text.hpp>

#include <string>

namespace grg {
class Graphic;
class Font;
/*
 *
 */
class GLabel: public GItem {
public:
	GLabel();
	GLabel(const std::string& text, const std::string& fontFileName);
	virtual ~GLabel();

	virtual void init();
	virtual void update(const int& frameDelay);
	virtual void draw(grg::Graphic& graphic) const;

	void SetFont(const std::string&);
	void SetFont(const grg::Font* font);

//	void setColor(const sf::Color& color);
//	void setText(const std::string& text);

//	virtual const grg::Font *GetFont() const;
	sf::Text* getTextOb();

	virtual void setOriginX(float xPos);
	virtual void setOriginY(float yPos);

	virtual void setXoffset(float xoffset);
	virtual void setYoffset(float yoffset);

	bool isCentered() const;
	void setCentered(bool centered);

//	virtual void setWidth(float width);
//	virtual void setHeight(float height);

protected:
	const grg::Font * mFont;
	sf::Text mTextOb;
	bool mCentered;

};

} /* namespace TileEngine */
#endif /* GLABEL_H_ */
