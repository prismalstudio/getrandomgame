/** 
 *  @file		GTextLine.h
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-05-31
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#ifndef GTEXTLINE_H_
#define GTEXTLINE_H_

#include <SFML/Graphics/Rect.hpp>

#include <vector>

namespace sf {
class Text;
}

namespace grg {
class Graphic;

/*
 *
 */
class GTextLine {
public:
	GTextLine(int maxWidth);
	virtual ~GTextLine();

	virtual unsigned int getWordCount() const;

	virtual sf::FloatRect getRect() const;
	virtual void setPosition(float x, float y);

	virtual bool pushWord(const sf::Text& word);

	virtual void Draw(grg::Graphic& graphic) const;

protected:
	std::vector<sf::Text> mWordVector;
	sf::FloatRect mRect;
	int mMaxWidth;

	virtual void updateLineRectDimension();
	virtual void updateTextPosition();
};

} /* namespace grg */
#endif /* GTEXTLINE_H_ */
