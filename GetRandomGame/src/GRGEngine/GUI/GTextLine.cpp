/** 
 *  @file		GTextLine.cpp
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-05-31
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#include "GTextLine.h"
#include "GRGEngine/Graphic/Graphic.h"

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

#include <iostream>
using std::cout;
using std::endl;

namespace grg {

GTextLine::GTextLine(int maxWidth) {
	mMaxWidth = maxWidth;
}

GTextLine::~GTextLine() {
	// TODO Auto-generated destructor stub
}

bool GTextLine::pushWord(const sf::Text& word) {

//	cout << "GTextLine::pushWord(" << (std::string) word.getString() << ")"
//			<< endl;

// prevent adding words to an already full line.
	if (mRect.width + word.getGlobalBounds().width >= mMaxWidth) {
		return false;
	}

	mWordVector.push_back(word);
	updateTextPosition();
	updateLineRectDimension();

	return true;
}

sf::FloatRect GTextLine::getRect() const {
	return mRect;
}

void GTextLine::setPosition(float x, float y) {
	mRect.left = x;
	mRect.top = y;
	updateTextPosition();
}

/**
 * Draws the words
 */
void GTextLine::Draw(grg::Graphic& graphic) const {
//	sf::RectangleShape wordTestRect;
//	wordTestRect.setFillColor(sf::Color::Transparent);
//	//wordTestRect.setOutlineColor(sf::Color::Red);
//	wordTestRect.setOutlineThickness(1);

	for (unsigned int i = 0; i < getWordCount(); ++i) {
		graphic.draw(mWordVector.at(i));

		//sf::FloatRect wordRect = mWordVector.at(i).getGlobalBounds();

//		sf::FloatRect wordRect = mWordVector.at(i).getLocalBounds();
//
//		wordTestRect.setSize(sf::Vector2f(wordRect.width, wordRect.height));
//		wordTestRect.setPosition(mWordVector.at(i).getPosition());
//
//		graphic.draw(wordTestRect);
	}

//	wordTestRect.setSize(sf::Vector2f(mRect.width, mRect.height));
//	wordTestRect.setPosition(mRect.left, mRect.top);
//
//	wordTestRect.setOutlineColor(sf::Color(0, 255, 0, 90));
//
//	graphic.draw(wordTestRect);
}

/**
 * @return the total number of words in that line
 */
unsigned int GTextLine::getWordCount() const {
	return mWordVector.size();
}

/**
 * Recount the height and width of the whole line.
 * Use after adding a word.
 */
void GTextLine::updateLineRectDimension() {
	mRect.width = 0;
	mRect.height = 0;
	for (unsigned int i = 0; i < getWordCount(); ++i) {
		mRect.width += mWordVector.at(i).getGlobalBounds().width;

		if (mWordVector.at(i).getGlobalBounds().height > mRect.height) {
			mRect.height = mWordVector.at(i).getGlobalBounds().height;
		}

	}

	// to get all the line inside, little hack
	mRect.height += mRect.height / 4;
}

/**
 * Adjust the position of each word.
 * Use after adjusting the position.
 */
void GTextLine::updateTextPosition() {
	float xMod = 0; // will keep the accumulated width of the previous words.

	// Go through all the words
	for (unsigned int i = 0; i < getWordCount(); ++i) {
		sf::FloatRect wordRect = mWordVector.at(i).getGlobalBounds();

		// adjust the height to be aligned to the middle in
		// case of different font or size
		//float height = mRect.top + (mRect.height / 2) - (wordRect.height / 2);
		mWordVector.at(i).setPosition(mRect.left + xMod, mRect.top);

		xMod += wordRect.width;

	}

}

} /* namespace grg */
