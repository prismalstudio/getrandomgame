/*
 * GMenuContainer.cpp
 *
 *  Created on: 2012-02-29
 *      Author: Emile
 */

#include "GuiContainer.h"
#include "GItem.h"
#include "GRGEngine/Graphic/Graphic.h"

#include <string>

using std::vector;

namespace grg {

GuiContainer::GuiContainer() {

}

GuiContainer::~GuiContainer() {
}

void GuiContainer::update(const int& frameDelay) {
	for (vector<GItem*>::iterator it = mItemList.begin(); it != mItemList.end();
			++it) {
		if ((*it)->isActivated()) {
			(*it)->update(frameDelay);
		}
	}

}

void GuiContainer::draw(grg::Graphic& graphic) const {
	for (unsigned int i = 0; i < mItemList.size(); ++i) {
		// for each Item, draw if active
		if (mItemList.at(i)->isActivated()) {
			mItemList.at(i)->draw(graphic);
		}
	}

}

/**
 * @param menuItem pointer to a menu Item
 *
 * Add a menu item in the update/draw list
 *
 */
void GuiContainer::pushGuiItem(GItem* menuItem) {
	if (!menuItem) {
		return; // prevent from adding NULL element
	}

	mItemList.push_back(menuItem);
	//mItemList.unique(); // prevent double

}

/**
 * Delete each GItem of the container
 */
void GuiContainer::clear() {
	for (unsigned i = mItemList.size(); i-- > 0;) {
		delete mItemList.at(i);
	}
	mItemList.clear();
}

} /* namespace grg */
