/*
 * GTextArea.h
 *
 *  Created on: 2012-03-13
 *      Author: Emile
 *
 *  TODO handle scrolling (than auto scrolling) in GTextArea.
 *  TODO add the possibility to append text to the top.
 *  TODO implement padding
 *
 *
 */

#ifndef GTEXTAREA_H_
#define GTEXTAREA_H_

#include "GItem.h"

#include <vector>
#include <string>

namespace sf {
class Text;
}

namespace grg {
class Graphic;
class Font;
class GTextLine;
/*
 *
 */
class GTextArea: public GItem {
public:
	GTextArea();
	virtual ~GTextArea();

	virtual void init();
	virtual void update(const int& frameDelay);

	virtual void draw(grg::Graphic& graphic) const;

	virtual void append(std::string text, const sf::Color& color =
			sf::Color::White, unsigned int style = 0,
			unsigned int characterSize = 20);
	virtual void appendLine(std::string text, const sf::Color& color =
			sf::Color::White, unsigned int style = 0,
			unsigned int characterSize = 20);
	virtual void endLine();

	virtual void updateLines();

	virtual void SetFont(std::string);
	virtual void SetFont(const grg::Font* font);

	virtual const grg::Font *GetFont() const;

	// override the base function to reset
	// each line pos/offset/pos
	virtual void setOriginX(float xPos);
	virtual void setOriginY(float yPos);

	virtual void setXoffset(float xoffset);
	virtual void setYoffset(float yoffset);

	virtual void setWidth(float width);
	virtual void setHeight(float height);

protected:
	const grg::Font* mFont;
	std::vector<grg::GTextLine> mLineVector;
	std::vector<std::string> mStringVector;
	GTextLine* mCurrentLine;
	bool mAutoScrolling;
//	Color3f *mColorDefault;

	virtual void pushLine(const grg::GTextLine & gTextLine);

};

} /* namespace TileEngine */
#endif /* GTEXTAREA_H_ */
