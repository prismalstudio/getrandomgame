/*
 * GMenuItem.h
 *
 *  Created on: 2012-02-27
 *      Author: Emile
 */

#ifndef GMENUITEM_H_
#define GMENUITEM_H_

#include <SFML/Graphics/RectangleShape.hpp>

#include <vector>

namespace grg {
class Graphic;
//class Event;

/*
 *
 */
class GItem {
public:
	GItem();
	GItem(float x, float y, float w, float h);
	virtual ~GItem() {

	}

	// Standard Object Interface
	virtual void init() = 0;
	virtual void update(const int&) = 0;
	virtual void draw(grg::Graphic& graphic) const = 0;

	// GItem methods
	virtual void pushItem(GItem*);
	virtual void toggle();

	/**
	 * ACCESSOR
	 *
	 */

	// Getter
	virtual float getAlpha() const;
	virtual bool isActivated() const;
	virtual bool isStatic() const;
	sf::RectangleShape & getBackground();

	virtual sf::Rect<float> getRect() const;
	virtual sf::Vector2<float> getPosition() const;
	virtual sf::Vector2<float> getOrigin() const;
	virtual sf::Vector2<float> getOffset() const;
	virtual sf::Vector2<float> getSize() const;

	// Setter 1 var
	virtual void setAlpha(float alpha, bool recurssive = true);
	virtual void setActivated(bool isActive);
	virtual void setStatic(bool isStatic);

	virtual void setOriginX(float xPos);
	virtual void setOriginY(float yPos);

	virtual void setXoffset(float xoffset);
	virtual void setYoffset(float yoffset);

	virtual void setWidth(float width);
	virtual void setHeight(float height);

	// Setter 2 vars
	virtual void setOrigin(float x, float y);
	virtual void setOffset(float x, float y);
	virtual void setSize(float w, float h);

	// Setter Vector2
	virtual void setOrigin(const sf::Vector2<float>& position);
	virtual void setOffset(const sf::Vector2<float>& offset);
	virtual void setSize(const sf::Vector2<float>& size);

protected:

	sf::Vector2<float> mOrigin;
	sf::Vector2<float> mOffset;
	sf::Rect<float> mRect;

	std::vector<GItem*> mItemVector;
	bool mStatic;
	bool mActivated;
	float mAlpha;

	sf::RectangleShape mBackground;

};

} /* namespace TileEngine */
#endif /* GMENUITEM_H_ */
