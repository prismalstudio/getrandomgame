/*
 * GButton.cpp
 *
 *  Created on: 2012-02-29
 *      Author: Emile
 */

#include "GButton.h"
#include "GRGEngine/Event/Event.h"
#include "GRGEngine/Graphic/Graphic.h"

#include <SFML/Window/Event.hpp>
#include <string>
#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

namespace grg {

GButton::GButton() {
	mActivatedEvent = NULL;
}
GButton::GButton(int x, int y, int w, int h) :
		GItem(x, y, w, h) {
	mActivatedEvent = NULL;
}
GButton::GButton(int x, int y, int w, int h, std::string text,
		std::string fontFilename) :
		GItem(x, y, w, h) {
	mActivatedEvent = NULL;
	mLabel = GLabel(text, fontFilename);
	mLabel.setCentered(true);
	mLabel.setOrigin(x + w / 2, y + h / 2);



	//mLabel.init();
}

GButton::~GButton() {
	delete mActivatedEvent;
}

void GButton::init() {
	GItem::init();
}

void GButton::update(const int& frameDelay) {
}

void GButton::draw(grg::Graphic& graphic) const {
	GItem::draw(graphic);
	mLabel.draw(graphic);

}

void GButton::setActivateEvent(grg::Event* event) {
	mActivatedEvent = event;
}

bool GButton::mouseButtonPressed(const sf::Event& event) {
	//cout << "GButton::mouseButtonPressed" << endl;
	int x = event.mouseButton.x;
	int y = event.mouseButton.y;
	//cout << "(" << x << ", " << y << ")" << endl;
	if (getRect().contains(x, y)
			&& event.mouseButton.button == sf::Mouse::Left) {
		cout << "GButton::MouseDown: INSIDE! " << endl;
		mActivatedEvent->throwEvent();
		return true;
	}
	return false;
//	cout << endl;
}

bool GButton::mouseButtonReleased(const sf::Event& event) {
	//cout << "GButton::mouseButtonReleased" << endl;
	return false;
}

bool GButton::mouseMoved(const sf::Event& event) {
	return false;
}

bool GButton::mouseWheelMoved(const sf::Event& event) {
	return false;
}

} /* namespace TileEngine */
