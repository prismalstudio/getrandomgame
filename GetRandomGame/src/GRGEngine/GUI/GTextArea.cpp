/*
 * GTextArea.cpp
 *
 *  Created on: 2012-03-13
 *      Author: Emile
 */

#include "GTextArea.h"
#include "GTextLine.h"

#include "GRGEngine/Resource/Font.h"
#include "GRGEngine/Graphic/Graphic.h"
#include "Util/Resource/TResourceManager.h"
#include "Util/StringExt.h"

#include <SFML/Graphics/Text.hpp>
#include <sstream>
#include <iostream>
using std::cout;
using std::endl;

using Util::TResourceManager;
using Util::split;

namespace grg {

/**
 * Constructor
 */
GTextArea::GTextArea() {
	//init();
	mFont = NULL;
	mCurrentLine = NULL;
	mAutoScrolling = false;
}

/**
 * Default desctructor
 */
GTextArea::~GTextArea() {
	delete mCurrentLine;
}

void GTextArea::init() {
	GItem::init();
}

void GTextArea::update(const int& frameDelay) {
}

/**
 * Draw
 * Render each line
 */
void GTextArea::draw(grg::Graphic& graphic) const {

	// draw generic stuff
	GItem::draw(graphic);

	// Get clipping working around the text Area
	bool initialClippingState = graphic.isClippingEnabled();

	if (!initialClippingState) {
		graphic.setClippingEnabled(true);
	}
	graphic.pushClippingArea(this->getRect());

	// draw the text
	for (unsigned int i = 0; i < mLineVector.size(); i++) {
		// TODO don't draw lines outside textarea
		mLineVector.at(i).Draw(graphic);
	}
	if (mCurrentLine) {
		mCurrentLine->Draw(graphic);
	}

	// then pop the rect
	graphic.popClippingArea();

	// and disable
	if (!initialClippingState) {
		graphic.setClippingEnabled(false);
	}

}

/**
 * Append to the current line
 */
void GTextArea::append(std::string text, const sf::Color& color,
		unsigned int style, unsigned int characterSize) {
	// TODO Implement append()
}

/**
 * Push the current line, push the new one.
 * Set the current line pointer to Null.
 */
void GTextArea::appendLine(std::string text, const sf::Color& color,
		unsigned int style, unsigned int characterSize) {

	//cout << "GTextArea::appendLine(" << text << ")" << endl;

	endLine(); // start on a new line.

	mCurrentLine = new (std::nothrow) GTextLine(this->getRect().width);

	// split the words
	std::vector<std::string> wordVector = split(text, ' ');

	// take each of them
	for (unsigned int i = 0; i < wordVector.size(); ++i) {

		// make a Text (drawable) object out of it (+ a space).
		sf::Text * currentWord = new (std::nothrow) sf::Text(
				wordVector.at(i) + " ", *mFont, characterSize);

		if (!currentWord) {
			std::cerr
					<< "GTextArea::appendLine: the currentWord pointer is still invalid, skipping it..."
					<< endl;
			continue; // skip it if the word went wrong
		}

		// set word options
		currentWord->setColor(color);
		currentWord->setStyle(style);

		// then tries to push it into the current line
		// if it isn't already full.
		if (!mCurrentLine->pushWord(*currentWord)) {
			// if it doesn't fit on the current line,
			// make sure that the word is not longer than the max width
			if (currentWord->getGlobalBounds().width >= getRect().width) {
				std::cerr
						<< "GTextArea::appendLine: single word longer than the TextArea..."
						<< endl;
				// the word is longer than the max width
				break;// TODO handle that case
			}
			// if full, push the line
			pushLine(*mCurrentLine);
			// create a new one
			mCurrentLine = new (std::nothrow) GTextLine(this->getRect().width);
			if (!mCurrentLine) {
				std::cerr
						<< "GTextArea::appendLine: failed to create a new GTextLine for the current line..."
						<< endl;
				return;
			}
			mCurrentLine->pushWord(*currentWord);

		}

	}

	endLine(); // push the line

} // end of appendLine

/**
 * Push the current line,
 * set the current line pointer to Null.
 */
void GTextArea::endLine() {
	if (mCurrentLine) {
		pushLine(*mCurrentLine);
	}

	mCurrentLine = NULL;
}

/**
 * Protected method, only for reuse purpose.
 */
void GTextArea::pushLine(const grg::GTextLine& gTextLine) {
	mLineVector.push_back(gTextLine);
	updateLines();
}

/**
 * Update the line with the new size/pos/offset of the Text Area.
 */
void GTextArea::updateLines() {
	int heightMod = 0;
	sf::Vector2f posCoord = getPosition();

	// update all the lines in the vector
	for (unsigned int i = 0; i < mLineVector.size(); ++i) {
		mLineVector.at(i).setPosition(posCoord.x, posCoord.y + heightMod);
		heightMod += mLineVector.at(i).getRect().height;
	}

	// then update the current line
	if (mCurrentLine) {
		mCurrentLine->setPosition(posCoord.x, posCoord.y + heightMod);
	}

}

void GTextArea::SetFont(std::string fontFileName) {
	SetFont(TResourceManager<grg::Font>::getInstance()->load(fontFileName));
}

void GTextArea::SetFont(const Font *font) {
	mFont = font;
//mSpaceWidth = mFont->GetWidth(" ") * (mPointSize / mFont->GetHeight());
}

const grg::Font* GTextArea::GetFont() const {
	return mFont;
}

void GTextArea::setOriginX(float xPos) {
	GItem::setOriginX(xPos);
	updateLines();
}

void GTextArea::setOriginY(float yPos) {
	GItem::setOriginY(yPos);
	updateLines();
}

void GTextArea::setXoffset(float xoffset) {
	GItem::setXoffset(xoffset);
	updateLines();
}

void GTextArea::setYoffset(float yoffset) {
	GItem::setYoffset(yoffset);
	updateLines();
}

void GTextArea::setWidth(float width) {
	GItem::setWidth(width);
	updateLines();
}

void GTextArea::setHeight(float height) {
	GItem::setHeight(height);
	updateLines();
}

} /* namespace TileEngine */
