/*
 * GMenuItem.cpp
 *
 *  Created on: 2012-02-27
 *      Author: Emile
 */

#include "GItem.h"
#include "GRGEngine/Event/Event.h"
#include "GRGEngine/Graphic/Graphic.h"
#include <string>

namespace grg {

GItem::GItem() {
	setOrigin(0, 0);
	setOffset(0, 0);
	setSize(0, 0);
	mActivated = true;
	mStatic = true;
	mAlpha = 1.0f;
	mBackground.setFillColor(sf::Color(0, 0, 0, 255 / 2));
}

GItem::GItem(float x, float y, float w, float h) {
	setOrigin(x, y);
	setOffset(0, 0);
	setSize(w, h);
	mActivated = true;
	mStatic = true;
	mAlpha = 1.0f;

	mBackground.setPosition(x, y);
	mBackground.setSize(sf::Vector2f(w, h));
	mBackground.setFillColor(sf::Color(0, 0, 0, 255 / 2));
}
void GItem::init() {

}

void GItem::update(const int& frameDelay) {
	for (unsigned int i = 0; i < mItemVector.size(); i++) {
		mItemVector.at(i)->update(frameDelay);
	}

}

void GItem::draw(grg::Graphic& graphic) const {
	if (mActivated) {
		// background, debug? or not
		graphic.draw(mBackground);
		// then all children components
		for (unsigned int i = 0; i < mItemVector.size(); i++) {
			mItemVector.at(i)->draw(graphic);
		}
	}
}

/**
 * @param menuItem pointer to a menu Item
 *
 * Add a menu item in the update/draw list
 *
 */
void GItem::pushItem(GItem *menuItem) {
	if (menuItem != NULL) {
		mItemVector.push_back(menuItem);
	}
}

void GItem::toggle() {
	setActivated(!isActivated());
}

/**
 * GETTER
 */

float GItem::getAlpha() const {
	return mAlpha;
}

bool GItem::isActivated() const {
	return mActivated;
}

bool GItem::isStatic() const {
	return mStatic;
}

sf::RectangleShape& GItem::getBackground() {
	return mBackground;
}

sf::Rect<float> GItem::getRect() const {
	return mRect;
}
sf::Vector2<float> GItem::getPosition() const {
	return mOrigin + mOffset;
}
sf::Vector2<float> GItem::getOrigin() const {
	return mOrigin;
}
sf::Vector2<float> GItem::getOffset() const {
	return mOffset;
}
sf::Vector2<float> GItem::getSize() const {
	return sf::Vector2<float>(mRect.width, mRect.height);
}

/**
 * SETTER 1 var
 */

void GItem::setAlpha(float alpha, bool recurssive) {
	float changedRatio = alpha / mAlpha;
	mAlpha = alpha;
	if (recurssive) {
		for (unsigned int i = 0; i < mItemVector.size(); i++) {
			mItemVector.at(i)->setAlpha(
					changedRatio * mItemVector.at(i)->getAlpha());
		}
	}
}
void GItem::setActivated(bool isActive) {
	mActivated = isActive;
}

void GItem::setStatic(bool isStatic) {
	mStatic = isStatic;
	for (unsigned int i = 0; i < mItemVector.size(); i++) {
		mItemVector.at(i)->setStatic(isStatic);
	}
}

/**
 * The item is at origin + offset, which gives you his "Position"
 * The offset is the distance from the origin.
 * The Position of a child Item is the origin + offset of This Item.
 * Each time the position or offset of "This" item is modified, each
 * child position must be updated.
 */
void GItem::setOriginX(float xPos) {

	mOrigin.x = xPos;
	mRect.left = getPosition().x;

	mBackground.setPosition(getPosition());

	for (unsigned int i = 0; i < mItemVector.size(); i++) {
		mItemVector.at(i)->setOriginX(getPosition().x);
	}
}
void GItem::setOriginY(float yPos) {
	mOrigin.y = yPos;
	mBackground.setPosition(getPosition());

	mRect.top = getPosition().y;

	for (unsigned int i = 0; i < mItemVector.size(); i++) {
		mItemVector.at(i)->setOriginY(getPosition().y);
	}
}
void GItem::setXoffset(float xoffset) {
	mOffset.x = xoffset;
	mBackground.setPosition(getPosition());

	mRect.left = getPosition().x;

	for (unsigned int i = 0; i < mItemVector.size(); i++) {
		mItemVector.at(i)->setOriginX(getPosition().x);
	}
}
void GItem::setYoffset(float yoffset) {
	mOffset.y = yoffset;
	mBackground.setPosition(getPosition());

	mRect.top = getPosition().y;

	for (unsigned int i = 0; i < mItemVector.size(); i++) {
		mItemVector.at(i)->setOriginY(getPosition().y);
	}
}

void GItem::setWidth(float width) {
	mRect.width = width;
	mBackground.setSize(getSize());
}
void GItem::setHeight(float height) {
	mRect.height = height;
	mBackground.setSize(getSize());
}

/**
 * SETTER 2 vars
 */

void GItem::setOrigin(float x, float y) {
	setOriginX(x);
	setOriginY(y);
}

void GItem::setOffset(float x, float y) {
	setXoffset(x);
	setYoffset(y);
}
void GItem::setSize(float w, float h) {
	setWidth(w);
	setHeight(h);
}

/**
 * SETTER Vector2
 */
void GItem::setOrigin(const sf::Vector2<float>& position) {
	setOriginX(position.x);
	setOriginY(position.y);
}
void GItem::setOffset(const sf::Vector2<float>& offset) {
	setXoffset(offset.x);
	setYoffset(offset.y);
}

void GItem::setSize(const sf::Vector2<float>& size) {
	setWidth(size.x);
	setHeight(size.y);
}

} /* namespace TileEngine */
