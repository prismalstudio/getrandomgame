/*
 * GButton.h
 *
 *  Created on: 2012-02-29
 *      Author: Emile
 */

#ifndef GBUTTON_H_
#define GBUTTON_H_

#include "GItem.h"
#include "GRGEngine/Event/ListenerInterface/IMouseListener.h"
#include "GLabel.h"
#include "GRGEngine/Graphic/Sprite.h"

namespace grg {
class Graphic;
class Event;
/*
 *
 */
class GButton: public GItem, public grg::IMouseListener {
public:
	GButton();
	GButton(int x, int y, int w, int h);
	GButton(int x, int y, int w, int h, std::string text,
			std::string fontFilename);
	virtual ~GButton();

	virtual void init();
	virtual void update(const int& frameDelay);
	virtual void draw(grg::Graphic& graphic) const;

	virtual void setActivateEvent(grg::Event * event);

	virtual bool mouseButtonPressed(const sf::Event& event);
	virtual bool mouseButtonReleased(const sf::Event& event);
	virtual bool mouseMoved(const sf::Event& event);
	virtual bool mouseWheelMoved(const sf::Event& event);

protected:
	grg::GLabel mLabel;
	grg::Sprite mBackground;
	grg::Event * mActivatedEvent;

};

} /* namespace grg */
#endif /* GBUTTON_H_ */
