/*
 * GLabel.cpp
 *
 *  Created on: 2012-02-29
 *      Author: Emile
 *
 *      @date	2013-05-21	changed to SFML
 */

#include "GLabel.h"
//#include "Graphic/Camera.h"
#include "GRGEngine/Resource/Font.h"
#include "GRGEngine/Graphic/Graphic.h"
#include "Util/Resource/TResourceManager.h"
#include <string>
#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

using Util::TResourceManager;

namespace grg {

GLabel::GLabel() :
		mFont(NULL) {
	//mFont = NULL;
	setCentered(false);
}

GLabel::GLabel(const std::string& text, const std::string& fontFileName) :
		mFont(NULL) {
	setCentered(false);
	SetFont(fontFileName);
	mTextOb.setString(text);
	mTextOb.setColor(sf::Color::Black);
	mRect = mTextOb.getGlobalBounds();
	setSize(mTextOb.getGlobalBounds().width, mTextOb.getGlobalBounds().height);

//	cout << "Position Back: " << getBackground().getPosition().x << ", "
//			<< getBackground().getPosition().y << endl;
//	cout << "Position Text: " << mTextOb.getPosition().x << ", "
//			<< mTextOb.getPosition().y << endl;

	getBackground().setOutlineColor(sf::Color::Green);
	getBackground().setFillColor(sf::Color::Transparent);
	getBackground().setOutlineThickness(2);
}

GLabel::~GLabel() {
}

void GLabel::init() {
	GItem::init();

	//mTextOb.setPosition(getPosition());

//	sf::FloatRect backRect = getBackground().getGlobalBounds();
//	sf::FloatRect textRect = mTextOb.getGlobalBounds();
//
//	cout << "Back: (" << backRect.left << ", " << backRect.top << ", "
//			<< backRect.width << ", " << backRect.height << ")" << endl;
//	cout << "Text: (" << textRect.left << ", " << textRect.top << ", "
//			<< textRect.width << ", " << textRect.height << ")" << endl;
//	cout << "GLabel: (" << getPosition().x << ", " << getPosition().y << ")"
//			<< endl;
}

void GLabel::update(const int& frameDelay) {
}

void GLabel::draw(grg::Graphic& graphic) const {
	//GItem::draw(graphic);
	graphic.draw(mTextOb);
}

/**
 * Uses the texture Manager template Singleton to load
 * the font if not already loaded. Then keep a pointer
 * to it through the Setter function.
 * @param font file name as a string
 */
void GLabel::SetFont(const std::string& fontFileName) {
	SetFont(TResourceManager<grg::Font>::getInstance()->load(fontFileName));
}

void GLabel::SetFont(const grg::Font* font) {
	mFont = font;
	mTextOb.setFont(*font);
	setSize(mTextOb.getGlobalBounds().width, mTextOb.getGlobalBounds().height);
}

void GLabel::setOriginX(float xPos) {
	//sf::Vector2<float> posV = getPosition();
	if (isCentered()) {
		//posV.x = xPos - getSize().x / 2;
		GItem::setOriginX(xPos - getSize().x / 2);
	} else {
//		posV.x = xPos;
		GItem::setOriginX(xPos);
	}
	mTextOb.setPosition(getPosition());
//	mTextOb.setPosition(posV);
//	GItem::setOriginX(mTextOb.getGlobalBounds().left);
}

/**
 * FIXME sf::Text global bounds aren't correct, it seems to be a bug.
 */
void GLabel::setOriginY(float yPos) {
	yPos -= 5; // FIXME corrective offset for the bug with text
//	sf::Vector2<float> posV = getPosition();
	if (isCentered()) {
//		posV.y = yPos - getSize().y / 2;
		GItem::setOriginY(yPos - getSize().y / 2);
	} else {
//		posV.y = yPos;
		GItem::setOriginY(yPos);
	}
	mTextOb.setPosition(getPosition());
//	mTextOb.setPosition(posV);
//	GItem::setOriginY(mTextOb.getGlobalBounds().top);
}

void GLabel::setXoffset(float xoffset) {
	GItem::setXoffset(xoffset);
	mTextOb.setPosition(getPosition());
}

void GLabel::setYoffset(float yoffset) {
	GItem::setYoffset(yoffset);
	mTextOb.setPosition(getPosition());
}

bool GLabel::isCentered() const {
	return mCentered;
}

//void GLabel::setColor(const sf::Color& color) {
//	mTextOb.setColor(color);
//}
//
//void GLabel::setText(const std::string& text) {
//	mTextOb.setString(text);
//}

sf::Text* GLabel::getTextOb() {
	return &mTextOb;
}

void GLabel::setCentered(bool centered) {
	mCentered = centered;
}

} /* namespace grg */
