/** 
 *  @file		FrameRateController.h
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-09-06
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#ifndef FRAMERATECONTROLLER_H_
#define FRAMERATECONTROLLER_H_

#include <SFML/System/Time.hpp>
#include <SFML/System/Clock.hpp>

#include <string>

namespace grg {

/*
 *
 */
class FrameRateController {
public:
	FrameRateController();
	virtual ~FrameRateController();

	void startFrame();
	bool hasTimeLeft();
	const sf::Time& update();
	void endFrame();

	const std::string& toString() const;
	const sf::Time& getElapsedTime() const;
	const sf::Time& getLastUpdate() const;
	const sf::Time& getTimePerFrame() const;
	void setTimePerFrame(const sf::Time& timePerFrame);

private:

	void updateStat();

	sf::Time mElapsedTime;
	sf::Time mLastUpdate;
	sf::Time mTimePerFrame;

	sf::Time mStatUpdateTime;
	std::size_t mStatNumFrames;
	std::size_t mStatNumUpdates;

	sf::Clock mClock;

	std::string mStatString;
};

} /* namespace grg */
#endif /* FRAMERATECONTROLLER_H_ */
