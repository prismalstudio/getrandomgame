/** 
 *  @file		Graphic.cpp
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-05-13
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#include "Graphic.h"

#include "GRGEngine/Resource/Texture.h"
#include "GRGEngine/Resource/Font.h"

//#include "Camera.h"
//#include "LightEngine.h"
//#include "GraphicType.h"
#include "GRGEngine/Engine.h"

#include "GRGEngine/Event/Event.h"

#include "Util/Resource/TResourceManager.h"

#include <SFML/Window/Event.hpp>
//#include <SFML/Window/WindowStyle.hpp>

#include <GL/gl.h>

#include <bitset>
#include <string>
#include <math.h> // sin cos whatever
#include <iostream>
using std::cout;
using std::endl;

using Util::TResourceManager;

using sf::FloatRect;

//const double PI = 4.0 * atan(1.0);

namespace grg
{

/**
 * default constructor
 */
Graphic::Graphic(std::string caption) :
				mStyleFlag(sf::Style::Default),
				mCurrentRenderTarget(NULL),
				mIsLoaded(false),
				mWidthScreen(800),
				mHeightScreen(600),
				mBpp(32),
				mWindowTitle(caption),
				mClippingEnabled(false),
				mDebugFont(NULL),
				mFullscreenEvent(new grg::Event(grg::Event::Fullsreen)),
				mWindowedEvent(new grg::Event(grg::Event::Windowed))
{
	mFullscreenEvent->size.width = getWidth();
	mFullscreenEvent->size.height = getHeight();

	mWindowedEvent->size.width = getWidth();
	mWindowedEvent->size.height = getHeight();
}

/**
 * default dtor
 */
Graphic::~Graphic()
{
	shutdown();
}

/**
 * Sets up a windows properties
 * must be called before CreateWindow towork properly
 * @param Width a int
 * @param Height a int
 * @param Bpp a int
 * @param WindowTitle a std::string
 */
bool Graphic::initialize(unsigned int Width, unsigned int Height,
		unsigned int Bpp, unsigned int style)
{
	cout << "Graphic::Init" << endl;
	mWidthScreen = Width;
	mHeightScreen = Height;
	mBpp = Bpp;
	mStyleFlag = style;

	//if we reinitalize then reload the textures
	if (mIsLoaded)
	{
		TResourceManager<grg::Texture>::getInstance()->reloadAll();
		TResourceManager<grg::Font>::getInstance()->reloadAll();
	}
	// debug font load
	mDebugFont = TResourceManager<grg::Font>::getInstance()->load(
			"asset/font/CaslonBold.ttf");

	// set the fullscreen bool
	// AND create (recreate) the window at the same time
	// since it is needed for the fullcreen setting.
	setFullscreen(mStyleFlag & sf::Style::Fullscreen);
	//mMainWindow.create(sf::VideoMode(Width, Height, Bpp), mWindowTitle, style);

	setCurrentRenderTarget(&mMainWindow);

	cout << "Graphic::Init END" << endl;
	return mIsLoaded;
}

void Graphic::draw(const sf::Drawable& drawable, const sf::RenderStates& states)
{
	getCurrentRenderTarget()->draw(drawable, states);
}

void Graphic::flipBuffer()
{
	mMainWindow.display();
}

void Graphic::reload()
{
}

sf::RenderWindow& Graphic::getWindow()
{
	return mMainWindow;
}

void Graphic::setCaption(std::string caption)
{
	mMainWindow.setTitle(caption);
}

int Graphic::getWidth() const
{
	return mMainWindow.isOpen() ? mMainWindow.getSize().x : mWidthScreen;
}

int Graphic::getHeight() const
{
	return mMainWindow.isOpen() ? mMainWindow.getSize().y : mHeightScreen;
}

void Graphic::setDefaultSize(unsigned int x, unsigned int y)
{
	if (x == 0 || y == 0)
	{
		// prevent from setting zero sized window
		return;
	}
	mWidthScreen = x;
	mHeightScreen = y;
}

void Graphic::clearBuffer()
{
	mMainWindow.clear();
}

const grg::Font* Graphic::getDebugFont() const
{
	return mDebugFont;
}

sf::RenderTarget* Graphic::getCurrentRenderTarget() const
{
	return mCurrentRenderTarget;
}

bool Graphic::isFullscreen() const
{
	return mStyleFlag & sf::Style::Fullscreen;
}

void Graphic::toggleFullscreen()
{
	setFullscreen(!isFullscreen());
}

void Graphic::setFullscreen(bool isFullscreen)
{
	if (isFullscreen)
	{
		mStyleFlag |= sf::Style::Fullscreen; // add the flag
		std::vector<sf::VideoMode> VModes = sf::VideoMode::getFullscreenModes();
		mMainWindow.create(VModes.at(0), mWindowTitle, mStyleFlag);

		mFullscreenEvent->size.width = getWidth();
		mFullscreenEvent->size.height = getHeight();
		mFullscreenEvent->throwEvent();
	}
	else
	{
		mStyleFlag &= ~(sf::Style::Fullscreen); // remove the flag
		mMainWindow.create(sf::VideoMode(mWidthScreen, mHeightScreen, mBpp),
				mWindowTitle, mStyleFlag);

		mWindowedEvent->size.width = getWidth();
		mWindowedEvent->size.height = getHeight();
		mWindowedEvent->throwEvent();
	}

	// debug outpu
	std::bitset<8> styleFlagBitset(mStyleFlag);
	cout << "Graphic::setFullscreen: new Window Styleflag value: "
			<< styleFlagBitset << endl;

}

// **************
// CLIPPING
// **************

/**
 * Creates a screen-sized rect for clipping if no clipping
 * rect in the stack. Else, return the top rect.
 */
sf::FloatRect Graphic::getCurrentClippingRect()
{
	FloatRect area;
	if (mClippingArea.empty())
	{
		area.left = 0.0f;
		area.top = 0.0f;
		area.height = getHeight();
		area.width = getWidth();
	}
	else
	{
		area = mClippingArea.top();
	}
	return area;
}

bool Graphic::isClippingEnabled() const
{
	return mClippingEnabled;
}

void Graphic::setClippingEnabled(bool clippingEnabled)
{
	mClippingEnabled = clippingEnabled;
	if (clippingEnabled)
	{
		// enable the scissor test and set the clipping rect
		glEnable(GL_SCISSOR_TEST);
		FloatRect area = getCurrentClippingRect();
		glScissor((GLsizei) (area.left),
				(GLsizei) (getHeight() - area.top - area.height),
				(GLint) (area.width), (GLint) (area.height));
	}
	else
	{
		glDisable(GL_SCISSOR_TEST);
	}
}

/**
 * Pushes a clipping area on the stack for drawing
 * @param area a sRect
 */
void Graphic::pushClippingArea(const FloatRect& area)
{
	FloatRect newArea;
	FloatRect currentArea = getCurrentClippingRect();

	//make the new clipping area from the rectangle intersection
	//top
	if (currentArea.top > area.top)
	{
		newArea.top = currentArea.top;
	}
	else
	{
		newArea.top = area.top;
	}
	//bottom
	if ((currentArea.top + currentArea.height) < (area.top + area.height))
	{
		newArea.height = (currentArea.top + currentArea.height) - newArea.top;
	}
	else
	{
		newArea.height = (area.top + area.height) - newArea.top;
	}
	//left
	if (currentArea.left > area.left)
	{
		newArea.left = currentArea.left;
	}
	else
	{
		newArea.left = area.left;
	}
	//right
	if ((currentArea.left + currentArea.width) < (area.left + area.width))
	{
		newArea.width = (currentArea.left + currentArea.width) - newArea.left;
	}
	else
	{
		newArea.width = (area.left + area.width) - newArea.left;
	}
	mClippingArea.push(newArea);
	glScissor((GLsizei) (newArea.left),
			(GLsizei) (getHeight() - newArea.top - newArea.height),
			(GLint) (newArea.width), (GLint) (newArea.height));
}

/**
 * Pops the last (top) clipping rect,
 * then set scissor test to the new top rect.
 */
void Graphic::popClippingArea()
{
	mClippingArea.pop();
	FloatRect newArea = getCurrentClippingRect();
	glScissor((GLsizei) (newArea.left),
			(GLsizei) (getHeight() - newArea.top - newArea.height),
			(GLint) (newArea.width), (GLint) (newArea.height));
}

void Graphic::shutdown()
{

}

void Graphic::setCurrentRenderTarget(sf::RenderTarget* currentRenderTarget)
{
	mCurrentRenderTarget = currentRenderTarget;
}

bool Graphic::closed(const sf::Event& event)
{
	return false;
}

bool Graphic::resized(const sf::Event& event)
{
	cout << "Graphic::resized to: (" << event.size.width << ", "
			<< event.size.height << ")" << endl;
	setDefaultSize(event.size.width, event.size.height);
	return false;
}

bool Graphic::gainedFocus(const sf::Event& event)
{
	return false;
}

bool Graphic::lostFocus(const sf::Event& event)
{
	return false;
}

bool Graphic::mouseEntered(const sf::Event& event)
{
	return false;
}

bool Graphic::mouseLeft(const sf::Event& event)
{
	return false;
}

} /* namespace grg */
