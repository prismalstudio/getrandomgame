/** 
 *  @file		Sprite.cpp
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-05-13
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#include "Sprite.h"
#include "GRGEngine/Graphic/GraphicBehavior/SpriteBehavior.h"
#include "GRGEngine/Resource/Texture.h"

#include "Util/Resource/TResourceManager.h"

using Util::TResourceManager;

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

namespace grg
{

Sprite::Sprite() :
				mGBehavior(NULL)
{

}

Sprite::Sprite(std::string filename) :
				mGBehavior(NULL)
{
	// load the texture
	grg::Texture * texture =
			TResourceManager<grg::Texture>::getInstance()->load(filename);
	if (!texture)
	{
		cerr << "Sprite::Sprite: tried to load [" << filename
				<< "] and it failed..." << endl;
	}

	setTexture(*texture);

}

Sprite::Sprite(const grg::Texture& texture) :
				sf::Sprite(texture),
				mGBehavior(NULL)
{
}

Sprite::Sprite(std::string filename, const sf::IntRect& rect) :
				mGBehavior(NULL)
{

	// load the texture
	grg::Texture * texture =
			TResourceManager<grg::Texture>::getInstance()->load(filename);
	if (!texture)
	{
		cerr << "Sprite::Sprite: tried to load [" << filename
				<< "] and it failed..." << endl;
	}

	setTexture(*texture);
	setTextureRect(rect);

}

Sprite::Sprite(const grg::Texture& texture, const sf::IntRect& rect) :
				sf::Sprite(texture, rect),
				mGBehavior(NULL)
{
}

Sprite::~Sprite()
{
	// TODO Auto-generated destructor stub
}

void Sprite::Update(const int& frameDelay)
{
	if(mGBehavior)
	{
		for (unsigned int i = 0; i < mGBehavior->size(); ++i)
		{
			if(mGBehavior->at(i))
			{
				mGBehavior->at(i)->update(frameDelay,*this);
			}
		}
	}
}

void Sprite::pushBehavior(SpriteBehavior* newGraphicBehavior)
{
	if(!newGraphicBehavior)
	{
		return;
	}

	if(!mGBehavior)
	{
		mGBehavior = new (std::nothrow)std::vector<SpriteBehavior*>();
	}

	mGBehavior->push_back(newGraphicBehavior);
}

} /* namespace grg */

