/*
 * GraphicBehavior.h
 *
 *  Created on: Aug 28, 2012
 *      Author: Emile
 */

#ifndef SPRITEBEHAVIOR_H_
#define SPRITEBEHAVIOR_H_

namespace grg {
class Sprite;
/*
 *
 */
class SpriteBehavior {
public:
	virtual ~SpriteBehavior() {

	}

	virtual void update(const int& frameTime, grg::Sprite& sprite) = 0;
};

} /* namespace grg */
#endif /* SPRITEBEHAVIOR_H_ */
