/** 
 *  @file		AnimationBehavior.h
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	Mar 22, 2013
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#ifndef ANIMATIONBEHAVIOR_H_
#define ANIMATIONBEHAVIOR_H_

#include "SpriteBehavior.h"

namespace grg {

/*
 *
 */
class AnimationBehavior: public grg::SpriteBehavior {
public:
	AnimationBehavior();
	AnimationBehavior(unsigned int frameDelay, unsigned int frameTotal,
			bool repeat = true, bool backward = false,
			bool seesawMotion = false);
	virtual ~AnimationBehavior();

	virtual void update(const int& frameTime, grg::Sprite& sprite);

	virtual void setFrame(unsigned int index);

	void reset();

	bool isPaused() const;
	bool isBackward() const;
	bool isRepeat() const;
	bool isSeesawMotion() const;

	void setPaused(bool paused);
	void setBackward(bool backward);
	void setRepeat(bool repeat);
	void setSeesawMotion(bool seesawMotion);

protected:
	unsigned int mFrameDelay;
	unsigned int mFrameIndex;
	unsigned int mFrameNbr;
	unsigned int mTimePassed;

	bool mRepeat;
	bool mBackward;
	bool mSeesawMotion;
	bool mPaused;
};

} /* namespace TileEngine */
#endif /* ANIMATIONBEHAVIOR_H_ */
