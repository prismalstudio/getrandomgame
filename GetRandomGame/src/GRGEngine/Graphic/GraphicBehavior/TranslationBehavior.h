/*
 * GraphicTranslation.h
 *
 *  Created on: Aug 28, 2012
 *      Author: Emile
 */

#ifndef GRAPHICTRANSLATION_H_
#define GRAPHICTRANSLATION_H_

#include "SpriteBehavior.h"

namespace grg {

/*
 *
 */
class TranslationBehavior: public grg::SpriteBehavior {
public:
	TranslationBehavior();
	TranslationBehavior(int xDestination, int yDestination,
			unsigned int timeToReach);
	virtual ~TranslationBehavior();

	virtual void update(const int& frameTime, grg::Sprite& sprite);

private:
	int mXoffset;
	int mYoffset;
	unsigned int mTimeToReach;
	unsigned int mTimeLeft;

};

} /* namespace TileEngine */
#endif /* GRAPHICTRANSLATION_H_ */
