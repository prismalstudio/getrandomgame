/*
 * GraphicTranslation.cpp
 *
 *  Created on: Aug 28, 2012
 *      Author: Emile
 */

// TODO update GraphicTranslation to work
#include "TranslationBehavior.h"

#include "GRGEngine/Graphic/Sprite.h"

namespace grg {

TranslationBehavior::TranslationBehavior() {
	mXoffset = 0;
	mYoffset = 0;
	mTimeToReach = 0;
	mTimeLeft = mTimeToReach;
}

TranslationBehavior::TranslationBehavior(int xDestination, int yDestination,
		unsigned int timeToReach) {
	mXoffset = xDestination;
	mYoffset = yDestination;
	mTimeToReach = timeToReach;
	mTimeLeft = mTimeToReach;
}

TranslationBehavior::~TranslationBehavior() {
	// TODO Auto-generated destructor stub
}

void TranslationBehavior::update(const int& frameTime, grg::Sprite& sprite) {
//	if (gameTime > (mFrameDelay + mLastFrame)) {
//		gData->adjustOffset(mXoffset, mYoffset);
//		mLastFrame = gameTime;
//	}
}

} /* namespace TileEngine */
