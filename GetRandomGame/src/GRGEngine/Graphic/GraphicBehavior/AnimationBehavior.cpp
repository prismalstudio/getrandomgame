/** 
 *  @file		AnimationBehavior.cpp
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	Mar 22, 2013
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

// TODO implement Seesaw Motion
#include "AnimationBehavior.h"

#include "GRGEngine/Graphic/Sprite.h"

#include <cmath>

namespace grg {

/**
 * default ctor
 */
AnimationBehavior::AnimationBehavior() :
		mFrameDelay(250), mFrameIndex(0), mFrameNbr(1), mTimePassed(0), mRepeat(
				true), mBackward(false), mSeesawMotion(false), mPaused(false) {

}

AnimationBehavior::AnimationBehavior(unsigned int frameDelay,
		unsigned int frameTotal, bool repeat, bool backward, bool seesawMotion) :
		mFrameDelay(frameDelay), mFrameIndex(0), mFrameNbr(frameTotal), mTimePassed(
				0), mRepeat(repeat), mBackward(backward), mSeesawMotion(
				seesawMotion), mPaused(false) {
}

AnimationBehavior::~AnimationBehavior() {
	// TODO Auto-generated destructor stub
}

void AnimationBehavior::reset() {
	mFrameIndex = 0;
	mTimePassed = 0;
}

/**
 *
 */
void AnimationBehavior::update(const int & frameTime, grg::Sprite& sprite) {
	if (!isPaused()) { // not paused
		// Time passed is the time left from last frame + the time taken this frame
		mTimePassed += frameTime;

		// Get the integer value only, gives you how many frame are to be increment.
		int frameMod = floor(((double) ((((mTimePassed))))) / mFrameDelay);
		// now readjust the time passed this frame by removing the time for the frame to be increment.
		mTimePassed -= frameMod * mFrameDelay;
		if (isBackward()) {
			setFrame(mFrameIndex - frameMod);
		} else {
			setFrame(mFrameIndex + frameMod);
		}
	}

	// update the gData clipping rectangle to clip the right frame
//	gData->getClippingRect().setX(
//			mFrameIndex * gData->getClippingRect().getW());

	sf::IntRect frameRect = sprite.getTextureRect();
	frameRect.left = mFrameIndex * frameRect.width;
	sprite.setTextureRect(frameRect);
}

/**
 * Set the current frame and avoid out of bound.
 * @param index a unsigned integer indicating the frame to switch to.
 */
void AnimationBehavior::setFrame(unsigned int index) {
	if ((!isRepeat()) && (index >= mFrameNbr)) {
		// prevent frame update when not repeating
		mFrameIndex = mFrameNbr - 1;
		setPaused(true);
	} else {
		// normal incrementation of frame while on repeat
		mFrameIndex = index % mFrameNbr;
	}
}

bool AnimationBehavior::isPaused() const {
	return mPaused;
}

void AnimationBehavior::setPaused(bool paused) {
	mPaused = paused;
}

bool AnimationBehavior::isBackward() const {
	return mBackward;
}

bool AnimationBehavior::isRepeat() const {
	return mRepeat;
}

bool AnimationBehavior::isSeesawMotion() const {
	return mSeesawMotion;
}

void AnimationBehavior::setBackward(bool backward) {
	mBackward = backward;
}

void AnimationBehavior::setRepeat(bool repeat) {
	mRepeat = repeat;
	// if we repeat and we are at the last frame already, reset the anim
	if (mRepeat && (mFrameIndex >= mFrameNbr - 1)) {
		reset();
	}
}

void AnimationBehavior::setSeesawMotion(bool seesawMotion) {
	mSeesawMotion = seesawMotion;
}

} /* namespace TileEngine */
