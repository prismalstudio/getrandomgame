/** 
 *  @file		Sprite.h
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-05-13
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#ifndef SPRITE_H_
#define SPRITE_H_

#include <SFML/Graphics/Sprite.hpp>

#include <vector>
#include <string>
#include "Graphic.h"

namespace grg {
class Texture;
class SpriteBehavior;
/*
 *
 */
class Sprite: public sf::Sprite {
public:
	Sprite();
	Sprite(std::string);
	Sprite(const grg::Texture&);
	Sprite(std::string, const sf::IntRect &);
	Sprite(const grg::Texture&, const sf::IntRect &);
	virtual ~Sprite();

	virtual void Update(const int& frameDelay);

	void draw(grg::Graphic& graphic);

	virtual void pushBehavior(SpriteBehavior *newGraphicBehavior);

protected:
	std::vector<SpriteBehavior*> * mGBehavior;
};

} /* namespace grg */
#endif /* SPRITE_H_ */
