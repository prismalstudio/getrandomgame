/** 
 *  @file		Graphic.h
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-05-13
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#ifndef GRAPHIC_H_
#define GRAPHIC_H_

//#include "Util/TSingleton.h"
//#include "GraphicType.h"

#include "GRGEngine/Event/ListenerInterface/IWindowListener.h"

//#include "GRGEngine/Event/Event.h"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Rect.hpp>

#include <string>
#include <stack>

namespace grg
{
class Font;
class Event;
/*
 *
 */
class Graphic: public grg::IWindowListener
{
	public:

		/// Default Constructor
		Graphic(std::string caption = "GRG game window");
		/// Default Destructor
		~Graphic();

		/// Intializes all a window for creation
		bool initialize(unsigned int width = 640, unsigned int height = 480,
				unsigned int bpp = 32, unsigned int style = sf::Style::Default);

		void draw(const sf::Drawable &drawable, const sf::RenderStates &states =
				sf::RenderStates::Default);
		/// kills all graphic subsystems
		void shutdown();

		/// Flips backbuffer to the screen
		void flipBuffer();

		/// Clear buffer
		void clearBuffer();

		/// Use when toggling fullscreen
		void reload();

		sf::RenderWindow & getWindow();
		const grg::Font * getDebugFont() const;

		void setCaption(std::string caption);

		int getWidth() const;
		int getHeight() const;
		void setDefaultSize(unsigned int x, unsigned int y);

		bool isFullscreen() const;
		void toggleFullscreen();
		void setFullscreen(bool isFullscreen);

		sf::FloatRect getCurrentClippingRect();
		bool isClippingEnabled() const;
		void setClippingEnabled(bool clippingEnabled);
		void pushClippingArea(const sf::FloatRect&);
		void popClippingArea();

		sf::RenderTarget* getCurrentRenderTarget() const;
		void setCurrentRenderTarget(sf::RenderTarget* currentRenderTarget);

		virtual bool closed(const sf::Event& event);
		virtual bool resized(const sf::Event& event);
		virtual bool gainedFocus(const sf::Event& event);
		virtual bool lostFocus(const sf::Event& event);

		virtual bool mouseEntered(const sf::Event& event);
		virtual bool mouseLeft(const sf::Event& event);

	private:

		sf::RenderWindow mMainWindow;
		sf::Uint32 mStyleFlag;

		sf::RenderTarget * mCurrentRenderTarget;

		bool mIsLoaded; ///< Tells if the graphics core was successfully loaded
		int mWidthScreen; ///< Stores the width of the drawing area
		int mHeightScreen; ///< Stores the height of the drawing area
		int mBpp; ///< Stores the bits per pixel of the screen
		std::string mWindowTitle; ///< Stores the title of the window

		std::stack<sf::FloatRect> mClippingArea;
		bool mClippingEnabled;

		const grg::Font * mDebugFont;

		grg::Event * mFullscreenEvent;
		grg::Event * mWindowedEvent;

};

} /* namespace grg */
#endif /* GRAPHIC_H_ */
