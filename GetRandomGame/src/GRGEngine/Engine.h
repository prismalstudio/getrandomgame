/*
 * 	@file TileEngine.h
 * 	Author: Emile
 *
 *      Generic Game loop and program flow controller.
 *
 *      To use:
 *      Init(width, height, caption, YourEnvironmentSubClass, YourInputHandlerSubClass);
 *
 *      // when you are ready:
 *      Start();
 *
 *		@date	2011-12-26 creation
 *      @date	2013-05-13 changed to sfml
 *
 */

#ifndef ENGINE_H_
#define ENGINE_H_

#include "GRGEngine/Event/EventManager/MouseEventManager.h"
#include "GRGEngine/Event/EventManager/KeyboardEventManager.h"
#include "GRGEngine/Event/EventManager/WindowEventManager.h"
#include "GRGEngine/Event/EventManager/MessageEventManager.h"

#include "FrameRateController.h"

#include "Util/TSingleton.h"

#include <SFML/Window/WindowStyle.hpp>

#include <string>
#include <vector>

namespace grg
{

// forward declaration
class AppState;
class Graphic;
class EventManager;

class Engine: public Util::TSingleton<Engine>,
		public grg::MouseEventManager,
		public grg::KeyboardEventManager,
		public grg::WindowEventManager,
		public grg::MessageEventManager
{
		friend class Util::TSingleton<Engine>;

	public:
		bool init(int screenW, int screenH, std::string caption,
				unsigned int windowStyle = sf::Style::Default);
		void reload();
		void start();
		void stop();

		void PushAppState(AppState* env);

		grg::Graphic * getGraphicOb() const;

		AppState* GetActiveAppState() const;

		void SetActiveAppState(unsigned int index);
		void NextAppState();

		void Exit(bool exit = true);
		bool isExit() const;
		int GetAppStateIndex() const;

	protected:
		Engine();
		~Engine();

		void Run();

		void SetAppStateIndex(unsigned int index);

		void handleInput();

		grg::Graphic * mGraphic;
		std::vector<AppState*> mvAppState;

		grg::FrameRateController mFpsController;
		int mAppStateIndex;
		bool mIsExit;
		bool mIsInit;

};
} /* namespace grg */
#endif /* ENGINE_H_ */
