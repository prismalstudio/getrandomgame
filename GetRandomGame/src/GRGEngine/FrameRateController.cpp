/** 
 *  @file		FrameRateController.cpp
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-09-06
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#include "FrameRateController.h"

#include "Util/StringExt.h"
using Util::toStr;

namespace grg {

FrameRateController::FrameRateController() :
				mElapsedTime(sf::Time::Zero),
				mLastUpdate(sf::Time::Zero),
				mTimePerFrame(sf::seconds(1.f / 60.f)),
				mStatNumFrames(0),
				mStatNumUpdates(0) {

}

FrameRateController::~FrameRateController() {
}

void FrameRateController::startFrame() {
	mElapsedTime = mClock.restart();
	mLastUpdate += mElapsedTime;
}

bool FrameRateController::hasTimeLeft() {
	return mLastUpdate > mTimePerFrame;
}

const sf::Time& FrameRateController::update() {
	++mStatNumUpdates;
	mLastUpdate -= mTimePerFrame;
	return mTimePerFrame;
}

void FrameRateController::endFrame() {
	updateStat();
}

void FrameRateController::updateStat() {
	mStatUpdateTime += getElapsedTime();
	mStatNumFrames += 1;

	if (mStatUpdateTime >= sf::seconds(1.0f)) {
		mStatString = "FramesPerSec: " + toStr(mStatNumFrames)
				+ ", UpdatesPerSec: " + toStr(mStatNumUpdates)
				+ ", AvgTimePerFrame = "
				+ toStr(mStatUpdateTime.asMicroseconds() / mStatNumFrames)
				+ "us";

		mStatUpdateTime -= sf::seconds(1.0f);
		mStatNumFrames = 0;
		mStatNumUpdates = 0;
	}
}

const std::string& FrameRateController::toString() const {
	return mStatString;
}

const sf::Time& FrameRateController::getElapsedTime() const {
	return mElapsedTime;
}

const sf::Time& FrameRateController::getLastUpdate() const {
	return mLastUpdate;
}

const sf::Time& FrameRateController::getTimePerFrame() const {
	return mTimePerFrame;
}

void FrameRateController::setTimePerFrame(const sf::Time& timePerFrame) {
	mTimePerFrame = timePerFrame;
}

} /* namespace grg */
