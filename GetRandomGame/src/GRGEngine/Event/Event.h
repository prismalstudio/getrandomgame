/*! 
 *  @Event.h
 *  @brief     
 *  @details   
 *  @author    Emile
 *  @date      Jan 31, 2013
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright Prismal Studio 2008-2013 www.prismalstudio.com
 */

#ifndef EVENT_H_
#define EVENT_H_

#include <string>

namespace grg
{

/*
 *
 */
class Event
{
	public:

		struct SizeEvent
		{
				unsigned int width;  ///< New width, in pixels
				unsigned int height; ///< New height, in pixels
		};

		struct AppStateEvent
		{
				unsigned int id;
		};

		struct UserEvent
		{
				int a;
				int b;
				char* msg;
		};

		/**
		 * @brief Defines an engine or user event and its parameters
		 */
		enum EventType
		{
			Fullsreen,
			Windowed,
			StateChanged,

			TotalCount, ///< keep last, total count of Event type

			USER_DEFINED
		};

	public:
		//Event();
		Event(EventType type);
		virtual ~Event();
		virtual void throwEvent();
		int getType() const;

		union
		{
				SizeEvent size; ///< Size event parameters (Event::Fullsreen and Windowed)
				AppStateEvent state; ///< Size event parameters (Event::StateChanged)
				UserEvent user; ///< Size event parameters (Event::UserEvent or higher)
		};

	protected:
		EventType mType;

};

} /* namespace TileEngine */
#endif /* EVENT_H_ */
