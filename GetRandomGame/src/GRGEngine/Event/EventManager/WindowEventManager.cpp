/** 
 *  @file		WindowEventManager.cpp
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-07-31
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#include "WindowEventManager.h"

#include "GRGEngine/Event/ListenerInterface/IWindowListener.h"

namespace grg {

WindowEventManager::WindowEventManager() {
	// TODO Auto-generated constructor stub

}

WindowEventManager::~WindowEventManager() {
	// TODO Auto-generated destructor stub
}

void WindowEventManager::addWindowListener(grg::IWindowListener* listener) {
	mvListener.push_back(listener);
}

void WindowEventManager::removeWindowListener(grg::IWindowListener* listener) {
	mvListener.remove(listener);
}

void WindowEventManager::clearWindowListener() {
	mvListener.clear();
}

bool WindowEventManager::emptyWindowListener() const {
	return mvListener.size() == 0;
}

void WindowEventManager::closed(const sf::Event& event) {
	std::list<grg::IWindowListener*>::iterator it;
	for (it = mvListener.begin(); it != mvListener.end(); ++it) {
		if ((*it)->closed(event)) {
			break;
		}
	}
}

void WindowEventManager::resized(const sf::Event& event) {
	std::list<grg::IWindowListener*>::iterator it;
	for (it = mvListener.begin(); it != mvListener.end(); ++it) {
		if ((*it)->resized(event)) {
			break;
		}
	}
}

void WindowEventManager::gainedFocus(const sf::Event& event) {
	std::list<grg::IWindowListener*>::iterator it;
	for (it = mvListener.begin(); it != mvListener.end(); ++it) {
		if ((*it)->gainedFocus(event)) {
			break;
		}
	}
}

void WindowEventManager::lostFocus(const sf::Event& event) {
	std::list<grg::IWindowListener*>::iterator it;
	for (it = mvListener.begin(); it != mvListener.end(); ++it) {
		if ((*it)->lostFocus(event)) {
			break;
		}
	}
}

void WindowEventManager::mouseEntered(const sf::Event& event) {
	std::list<grg::IWindowListener*>::iterator it;
	for (it = mvListener.begin(); it != mvListener.end(); ++it) {
		if ((*it)->mouseEntered(event)) {
			break;
		}
	}
}

void WindowEventManager::mouseLeft(const sf::Event& event) {
	std::list<grg::IWindowListener*>::iterator it;
	for (it = mvListener.begin(); it != mvListener.end(); ++it) {
		if ((*it)->mouseLeft(event)) {
			break;
		}
	}
}

} /* namespace grg */
