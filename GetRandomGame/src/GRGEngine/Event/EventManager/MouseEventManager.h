/** 
 *  @file		MouseEventManager.h
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-07-31
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#ifndef MOUSEEVENTMANAGER_H_
#define MOUSEEVENTMANAGER_H_

#include <list>

namespace sf {
class Event;
}

namespace grg {
class IMouseListener;
/*
 *
 */
class MouseEventManager {
public:
	MouseEventManager();
	virtual ~MouseEventManager();

	void addMouseListener(grg::IMouseListener * listener);
	void removeMouseListener(grg::IMouseListener * listener);

	void clearMouseListener();

	bool emptyMouseListener() const;

protected:
	void mouseButtonPressed(const sf::Event& event);
	void mouseButtonReleased(const sf::Event& event);
	void mouseMoved(const sf::Event& event);
	void mouseWheelMoved(const sf::Event& event);

private:
	std::list<grg::IMouseListener*> mvListener;

};

} /* namespace grg */
#endif /* MOUSEEVENTMANAGER_H_ */
