/** 
 *  @file		MouseEventManager.cpp
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-07-31
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#include "MouseEventManager.h"

#include "GRGEngine/Event/ListenerInterface/IMouseListener.h"

namespace grg {

MouseEventManager::MouseEventManager() {
	// TODO Auto-generated constructor stub

}

MouseEventManager::~MouseEventManager() {
	// TODO Auto-generated destructor stub
}

void MouseEventManager::addMouseListener(grg::IMouseListener* listener) {
	mvListener.push_back(listener);
}

void MouseEventManager::removeMouseListener(grg::IMouseListener* listener) {
	mvListener.remove(listener);
}

void MouseEventManager::clearMouseListener() {
	mvListener.clear();
}

bool MouseEventManager::emptyMouseListener() const {
	return mvListener.size() == 0;
}

void MouseEventManager::mouseButtonPressed(const sf::Event& event) {
	std::list<grg::IMouseListener*>::iterator it;
	for (it = mvListener.begin(); it != mvListener.end(); ++it) {
		if ((*it)->mouseButtonPressed(event)) {
			break;
		}
	}
}

void MouseEventManager::mouseButtonReleased(const sf::Event& event) {
	std::list<grg::IMouseListener*>::iterator it;
	for (it = mvListener.begin(); it != mvListener.end(); ++it) {
		if ((*it)->mouseButtonReleased(event)) {
			break;
		}
	}
}

void MouseEventManager::mouseMoved(const sf::Event& event) {
	std::list<grg::IMouseListener*>::iterator it;
	for (it = mvListener.begin(); it != mvListener.end(); ++it) {
		if ((*it)->mouseMoved(event)) {
			break;
		}
	}
}

void MouseEventManager::mouseWheelMoved(const sf::Event& event) {
	std::list<grg::IMouseListener*>::iterator it;
	for (it = mvListener.begin(); it != mvListener.end(); ++it) {
		if ((*it)->mouseWheelMoved(event)) {
			break;
		}
	}
}

} /* namespace grg */
