/** 
 *  @file		KeyboardEventManager.cpp
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-07-31
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#include "KeyboardEventManager.h"

#include "GRGEngine/Event/ListenerInterface/IKeyboardListener.h"

namespace grg {

KeyboardEventManager::KeyboardEventManager() {
	// TODO Auto-generated constructor stub

}

KeyboardEventManager::~KeyboardEventManager() {
	// TODO Auto-generated destructor stub
}

void KeyboardEventManager::addKeyboardListener(
		grg::IKeyboardListener* listener) {
	mvListener.push_back(listener);
}

void KeyboardEventManager::removeKeyboardListener(
		grg::IKeyboardListener* listener) {
	mvListener.remove(listener);
}

void KeyboardEventManager::clearKeyboardListener() {
	mvListener.clear();
}

bool KeyboardEventManager::emptyKeyboardListenerList() const {
	return mvListener.size() == 0;
}

void KeyboardEventManager::keyPressed(const sf::Event& event) {
	std::list<grg::IKeyboardListener*>::iterator it;
	for (it = mvListener.begin(); it != mvListener.end(); ++it) {
		if ((*it)->keyPressed(event)) {
			break;
		}
	}
}

void KeyboardEventManager::keyReleased(const sf::Event& event) {
	std::list<grg::IKeyboardListener*>::iterator it;
	for (it = mvListener.begin(); it != mvListener.end(); ++it) {
		if ((*it)->keyReleased(event)) {
			break;
		}
	}
}

void KeyboardEventManager::textEntered(const sf::Event& event) {
	std::list<grg::IKeyboardListener*>::iterator it;
	for (it = mvListener.begin(); it != mvListener.end(); ++it) {
		if ((*it)->textEntered(event)) {
			break;
		}
	}
}

} /* namespace grg */
