/** 
 *  @file		MessageEventManager.h
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-07-31
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#ifndef MESSAGEEVENTMANAGER_H_
#define MESSAGEEVENTMANAGER_H_

#include <list>
#include <map>
#include <queue>

namespace grg {
class IMessageListener;
class Event;
/*
 *
 */
class MessageEventManager {
public:
	MessageEventManager();
	virtual ~MessageEventManager();

	void addMessageListener(const int& type, grg::IMessageListener * listener);
	void removeMessageListener(const int& type,
			grg::IMessageListener * listener);

	void clearMessageListener();

	bool emptyMessageListener() const;

	void handleMessage(grg::Event * event);

protected:
	void sendMessageQueue();
private:
	std::map<int, std::list<grg::IMessageListener*> > mvMessageListener;
	std::queue<grg::Event*> * mEventQueue;
};

} /* namespace grg */
#endif /* MESSAGEEVENTMANAGER_H_ */
