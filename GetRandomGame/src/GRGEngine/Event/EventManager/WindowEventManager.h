/** 
 *  @file		WindowEventManager.h
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-07-31
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#ifndef WINDOWEVENTMANAGER_H_
#define WINDOWEVENTMANAGER_H_

#include <list>

namespace sf {
class Event;
}

namespace grg {
class IWindowListener;
/*
 *
 */
class WindowEventManager {
public:
	WindowEventManager();
	virtual ~WindowEventManager();

	void addWindowListener(grg::IWindowListener * listener);
	void removeWindowListener(grg::IWindowListener * listener);

	void clearWindowListener();

	bool emptyWindowListener() const;
protected:
	void closed(const sf::Event& event);
	void resized(const sf::Event& event);
	void gainedFocus(const sf::Event& event);
	void lostFocus(const sf::Event& event);

	void mouseEntered(const sf::Event& event);
	void mouseLeft(const sf::Event& event);

private:
	std::list<grg::IWindowListener*> mvListener;
};

} /* namespace grg */
#endif /* WINDOWEVENTMANAGER_H_ */
