/** 
 *  @file		MessageEventManager.cpp
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-07-31
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#include "MessageEventManager.h"

#include "GRGEngine/Event/ListenerInterface/IMessageListener.h"
#include "GRGEngine/Event/Event.h"

#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

namespace grg
{

MessageEventManager::MessageEventManager()
{
	mEventQueue = new (std::nothrow) std::queue<grg::Event*>();

}

MessageEventManager::~MessageEventManager()
{
	// TODO Auto-generated destructor stub
}

void MessageEventManager::addMessageListener(const int& type,
		grg::IMessageListener* listener)
{
	// if the type isn't in the map
	if (mvMessageListener.find(type) == mvMessageListener.end())
	{
		// add it.
		mvMessageListener[type] = std::list<IMessageListener*>();
	}
	// then add the listener to the list.
	mvMessageListener[type].push_back(listener);
}

void MessageEventManager::removeMessageListener(const int& type,
		grg::IMessageListener* listener)
{
	// if the type isn't in the map
	if (mvMessageListener.find(type) == mvMessageListener.end())
	{
		// no need to remove it, it's not there
		return;
	}
	// then add the listener to the list.
	mvMessageListener[type].remove(listener);
}

void MessageEventManager::clearMessageListener()
{
	mvMessageListener.clear();
}

bool MessageEventManager::emptyMessageListener() const
{
	return mvMessageListener.size() == 0;
}

void MessageEventManager::handleMessage(grg::Event * event)
{
	if (mEventQueue)
	{
		mEventQueue->push(event);
	}
}

void MessageEventManager::sendMessageQueue()
{

	std::queue<grg::Event*> * curQueue = mEventQueue;
	mEventQueue = new (std::nothrow) std::queue<grg::Event*>();

	// for all event in the queue
	while (!curQueue->empty())
	{
		grg::Event event = *curQueue->front();

		// if the type isn't in the map
		if (mvMessageListener.find(event.getType()) == mvMessageListener.end())
		{
			// that means no listener, so no object wants to hear about it.
			// no need to handle it, just return.
			cerr << "EventManager::handleEvent: No listener for event of type: "
					<< event.getType() << endl;
			// then pop it, nobody wants to hear about it anyway.
		}
		else
		{

			// handle the event
			std::list<IMessageListener*> listenerList =
					mvMessageListener[event.getType()];
			std::list<IMessageListener*>::iterator it;

			// send it to all the listener for the type
			for (it = listenerList.begin(); it != listenerList.end(); ++it)
			{
				if ((*it)->handleMessage(event))
				{
					break; // event handled
				}
			}
		}

		curQueue->pop();
	}

	delete curQueue;
}

} /* namespace grg */
