/** 
 *  @file		KeyboardEventManager.h
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-07-31
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#ifndef KEYBOARDEVENTMANAGER_H_
#define KEYBOARDEVENTMANAGER_H_

#include <list>

namespace sf {
class Event;
}

namespace grg {
class IKeyboardListener;
/*
 *
 */
class KeyboardEventManager {
public:
	KeyboardEventManager();
	virtual ~KeyboardEventManager();

	void addKeyboardListener(grg::IKeyboardListener * listener);
	void removeKeyboardListener(grg::IKeyboardListener * listener);

	void clearKeyboardListener();

	bool emptyKeyboardListenerList() const;

protected:
	void keyPressed(const sf::Event& event);
	void keyReleased(const sf::Event& event);
	void textEntered(const sf::Event& event);
private:
	std::list<grg::IKeyboardListener*> mvListener;
};

} /* namespace grg */
#endif /* KEYBOARDEVENTMANAGER_H_ */
