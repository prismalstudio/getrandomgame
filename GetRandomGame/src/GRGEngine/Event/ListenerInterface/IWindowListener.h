/** 
 *  @file		IWindowListener.h
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-07-31
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#ifndef IWINDOWLISTENER_H_
#define IWINDOWLISTENER_H_

namespace sf {
class Event;
}

namespace grg {

/*
 *
 */
class IWindowListener {
public:
	virtual ~IWindowListener() {

	}

	virtual bool closed(const sf::Event& event) =0;
	virtual bool resized(const sf::Event& event) =0;
	virtual bool gainedFocus(const sf::Event& event) =0;
	virtual bool lostFocus(const sf::Event& event) =0;

	virtual bool mouseEntered(const sf::Event& event) =0;
	virtual bool mouseLeft(const sf::Event& event) =0;
};

} /* namespace grg */
#endif /* IWINDOWLISTENER_H_ */
