/** 
 *  @file		IKeyboardListener.h
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-07-31
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#ifndef IKEYBOARDLISTENER_H_
#define IKEYBOARDLISTENER_H_

namespace sf {
class Event;
}

namespace grg {

/*
 *
 */
class IKeyboardListener {
public:
	virtual ~IKeyboardListener() {

	}

	virtual bool keyPressed(const sf::Event& event) =0;
	virtual bool keyReleased(const sf::Event& event) =0;
	virtual bool textEntered(const sf::Event& event) =0;
};

} /* namespace grg */
#endif /* IKEYBOARDLISTENER_H_ */
