/** 
 *  @file		IMessageListener.h
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-07-31
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#ifndef IMESSAGELISTENER_H_
#define IMESSAGELISTENER_H_

namespace grg {
class Event;
/*
 *
 */
class IMessageListener {
public:
	virtual ~IMessageListener() {
	}

	virtual bool handleMessage(const grg::Event& event) =0;
};

} /* namespace grg */
#endif /* IMESSAGELISTENER_H_ */
