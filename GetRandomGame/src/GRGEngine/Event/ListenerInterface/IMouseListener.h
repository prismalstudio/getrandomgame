/** 
 *  @file		IMouseListener.h
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-07-31
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#ifndef IMOUSELISTENER_H_
#define IMOUSELISTENER_H_

namespace sf {
class Event;
}

namespace grg {

/*
 *
 */
class IMouseListener {
public:
	virtual ~IMouseListener() {
	}

	virtual bool mouseButtonPressed(const sf::Event& event) =0;
	virtual bool mouseButtonReleased(const sf::Event& event) =0;
	virtual bool mouseMoved(const sf::Event& event) =0;
	virtual bool mouseWheelMoved(const sf::Event& event) =0;

};

} /* namespace grg */
#endif /* IMOUSELISTENER_H_ */
