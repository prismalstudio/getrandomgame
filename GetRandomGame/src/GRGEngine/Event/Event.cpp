/*! 
 *  @Event.cpp
 *  @brief     
 *  @details   
 *  @author    Emile
 *  @date      Jan 31, 2013
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright Prismal Studio 2008-2013 www.prismalstudio.com
 */

#include "Event.h"

#include "GRGEngine/Engine.h"

#include <iostream>
using std::cout;
using std::endl;

namespace grg
{

Event::Event(EventType type)
{
	mType = type;
}

Event::~Event()
{
	// TODO Auto-generated destructor stub
}

void Event::throwEvent()
{
	cout << "Event::throwEvent() type : " << getType() << endl;
	//Engine::getInstance()->getEventManager()->handleEvent(*this);
	Engine::getInstance()->handleMessage(this);
}

int Event::getType() const
{
	return mType;
}

} /* namespace TileEngine */
