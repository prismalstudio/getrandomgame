/** 
 *  @file		Font.h
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-05-13
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#ifndef FONT_H_
#define FONT_H_

#include "Util/Resource/Resource.h"

#include <SFML/Graphics/Font.hpp>

namespace grg {

/*
 *
 */
class Font: public Util::Resource, public sf::Font {
public:
	Font();
	virtual ~Font();

	/// Loads a font from file
	virtual bool load(const std::string& filename);
};

} /* namespace grg */
#endif /* FONT_H_ */
