/** 
 *  @file		Font.cpp
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-05-13
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#include "Font.h"

namespace grg {

Font::Font() {
}

Font::~Font() {
}

bool Font::load(const std::string& filename) {
	//mFilename = filename; // Ressource filename
	setFilename(filename);
	setLoaded(loadFromFile(filename));

	return isLoaded();
}

} /* namespace grg */

