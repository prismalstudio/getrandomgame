/** 
 *  @file		Texture.h
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-05-13
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#ifndef TEXTURE_H_
#define TEXTURE_H_

#include "Util/Resource/Resource.h"

#include <SFML/Graphics/Texture.hpp>

namespace grg {

/*
 *
 */
class Texture: public Util::Resource, public sf::Texture {
public:
	Texture();
	virtual ~Texture();

	virtual bool load(const std::string& filename);

	unsigned int GetTexture() const;
};

} /* namespace grg */
#endif /* TEXTURE_H_ */
