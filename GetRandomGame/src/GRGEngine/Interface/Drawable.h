/** 
 *  @file		Drawable.h
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-08-28
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#ifndef DRAWABLE_H_
#define DRAWABLE_H_

namespace grg {
class Graphic;
/*
 *
 */
class Drawable {
public:
	virtual ~Drawable() {

	}

	virtual void draw(grg::Graphic& graphic) const = 0;
};

} /* namespace grg */
#endif /* DRAWABLE_H_ */
