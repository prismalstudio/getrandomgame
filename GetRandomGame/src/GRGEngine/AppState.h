/** 
 *  @file		AppState.h
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-05-13
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#ifndef APPSTATE_H_
#define APPSTATE_H_


namespace grg {
class Graphic;
class Engine;

/*
 *
 */
class AppState {
public:
	AppState(Engine * eng);
	virtual ~AppState();

	// Generic Environment Methods
	virtual bool Init() = 0; // generic initialization
	virtual void Reload() = 0;
	virtual void Update(const int&) = 0; // generic function, redefine by the subclass
	virtual void Draw(grg::Graphic&) = 0; // generic function, redefine by the subclass
	virtual void Close() = 0; // generic function, redefine by the subclass

	bool isInit() const;

protected:

	void setInit(bool isInit);

	bool mIsInit;
	Engine * mEngine;
};

} /* namespace TileEngine */
#endif /* APPSTATE_H_ */
