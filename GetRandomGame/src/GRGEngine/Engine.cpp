/*!
 * @TileEngine.cpp
 *
 *  @date 2011-12-26
 *  @author Emile
 */

#include "Engine.h"
#include "AppState.h"
#include "Graphic/Graphic.h"
#include "Resource/Texture.h"
#include "Resource/Font.h"
#include "Util/Resource/TResourceManager.h"
#include "Util/StringExt.h"

#include <SFML/Window/Event.hpp>

#include <string>
#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

using Util::TResourceManager;

namespace grg
{

/****************************************
 * CONSTRUTOR
 */
Engine::Engine() :
				mGraphic(NULL),
				mAppStateIndex(0),
				mIsExit(false),
				mIsInit(false)
{
}

/****************************************
 *  Call that first, then call start();
 *  it sets the default values and Screen size/dimensions
 *  @param screenW
 *  @param
 *
 */
bool Engine::init(int screenW, int screenH, std::string caption,
		unsigned int windowStyle)
{
	cout << "Engine init" << endl;
	mIsInit = false;

	mGraphic = new (std::nothrow) grg::Graphic(caption);
	mGraphic->initialize(screenW, screenH, 32, windowStyle);

	addWindowListener(mGraphic);
	//mInputHandler = inputHandler;
	mIsExit = false;

	// init the first Environment
	if (!mvAppState.empty())
	{
		if (GetActiveAppState()->Init())
		{
			// if init went well, the Engine is Init
			mIsInit = true;
		}
		else
		{
			cerr << "Initialization of active app state went wrong..." << endl;
		}
	}
	else
	{
		cerr << "Engine has no app state to load and/or init..." << endl;
	}

	cout << "Engine init::End" << endl;
	return mIsInit;
}

void Engine::reload()
{
	GetActiveAppState()->Reload();
}

/****************************************
 * start()
 *  It literally starts the engine
 *
 */
void Engine::start()
{
	cout << "Engine start" << endl;
	if (mIsInit)
	{
		Run(); //start the game loop
	}
	else
	{
		cerr
				<< "Engine::Start(): Error, please call Engine::Init before start()..."
				<< endl;
	}
	cout << "Engine start::End" << endl;
}

/****************************************
 * run()
 *  its the actual app loop
 *
 */
void Engine::Run()
{
	cout << "Engine Run()" << endl;

	/**
	 *  GAME LOOP
	 */
	while (!isExit())
	{
		/**
		 * each iteration, do this:
		 */
		//cout << "start frame" << endl;
		mFpsController.startFrame();

		while (mFpsController.hasTimeLeft())
		{
			handleInput();

			sendMessageQueue();

			//GetActiveAppState()->Update(getFrameDelay()); // UPDATE the environment
			GetActiveAppState()->Update(
					mFpsController.update().asMilliseconds()); // UPDATE the environment

		}

		mFpsController.endFrame();

		mGraphic->setCaption(mFpsController.toString()); // DEBUG
		mGraphic->clearBuffer();
		//setFrameDelay(mFrameTimer.restart().asMilliseconds()); //Calcul le temps d'execution de l'iteration

		GetActiveAppState()->Draw(*mGraphic); // Do all the draw OPs FIRST
		mGraphic->flipBuffer(); // flip the screen

	} // End of the GAME LOOP

	// Stop the program correctly
	stop();
	cout << "Engine Run()::End" << endl;
}

void Engine::handleInput()
{
	sf::Event event;
	// Key event : This "if" returns true only if a key have been pressed
	while (mGraphic->getWindow().pollEvent(event))
	{
		//cout << "GRGInputHandler::handleInput Event: " << event.type << endl;

		switch (event.type)
		{
		case sf::Event::Closed:
			closed(event);
			Exit();
			break;
		case sf::Event::Resized:
			resized(event);
			break;
		case sf::Event::GainedFocus:
			gainedFocus(event);
			break;
		case sf::Event::LostFocus:
			lostFocus(event);
			break;
		case sf::Event::MouseEntered:
			mouseEntered(event);
			break;
		case sf::Event::MouseLeft:
			mouseLeft(event);
			break;
		case sf::Event::KeyPressed:
			keyPressed(event);
			break;
		case sf::Event::KeyReleased:
			keyReleased(event);
			break;
		case sf::Event::TextEntered:
			textEntered(event);
			break;
		case sf::Event::MouseButtonPressed:
			mouseButtonPressed(event);
			break;
		case sf::Event::MouseButtonReleased:
			mouseButtonReleased(event);
			break;
		case sf::Event::MouseMoved:
			mouseMoved(event);
			break;
		case sf::Event::MouseWheelMoved:
			mouseWheelMoved(event);
			break;
		default:
			cerr << "Engine::handleInput() unhandle event of type: "
					<< event.type << endl;
			break;

		}

	} // end here if no keys is pressed

}

/**
 * TODO Create an AppState Manager for these methods
 */
void Engine::PushAppState(AppState *env)
{
	mvAppState.push_back(env);
}

AppState *Engine::GetActiveAppState() const
{
	return mvAppState.at(GetAppStateIndex());
}

/**
 * SetActiveEnvironment close the active environment and then change the
 * current environment index (can't be negative, also protected by modulo for
 * out of bound) and init that new setted environment.
 * @param index the positive integer index of the next environment.
 */
void Engine::SetActiveAppState(unsigned int index)
{
	// first close the current environment
	GetActiveAppState()->Close();

	clearKeyboardListener();
	clearMessageListener();
	clearMouseListener();
	clearWindowListener();

	// make sure the index is in bound
	SetAppStateIndex(index);
	GetActiveAppState()->Init();

	// DEBUG temporary
	addWindowListener(mGraphic);
}

/**
 *
 */
void Engine::NextAppState()
{
	cout << "Engine::NextEnvironment() index: " << GetAppStateIndex() << endl;
	cout << "Env vector size: " << mvAppState.size() << endl;
	SetActiveAppState(GetAppStateIndex() + 1);
}

void Engine::SetAppStateIndex(unsigned int index)
{
	mAppStateIndex = index % mvAppState.size();
}

grg::Graphic* Engine::getGraphicOb() const
{
	return mGraphic;
}

int Engine::GetAppStateIndex() const
{
	return mAppStateIndex;
}

/****************************************
 *
 *
 */
void Engine::stop()
{
	cout << "Engine::stop()" << endl;
	GetActiveAppState()->Close();
	mGraphic->shutdown();
	TResourceManager<Texture>::getInstance()->deleteAll();
	TResourceManager<Texture>::getInstance()->kill();
	TResourceManager<Font>::getInstance()->deleteAll();
	TResourceManager<Font>::getInstance()->kill();
	cout << "Engine::stop()::END" << endl;
}

void Engine::Exit(bool exit)
{
	mIsExit = exit;
}
bool Engine::isExit() const
{
	return mIsExit;
}

Engine::~Engine()
{
	stop();
}

} // end of namespace TileEngine

