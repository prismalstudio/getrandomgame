/** 
 *  @file		AppState.cpp
 *  @brief     
 *  @details   
 *  @author    	Emile
 *  @date      	2013-05-13
 *  @pre       
 *  @bug       
 *  @warning   
 *  @copyright 	Prismal Studio 2008-2013 www.prismalstudio.com
 */

#include "AppState.h"

namespace grg {

AppState::AppState(Engine * eng) :
				mIsInit(false),
				mEngine(eng){
}

AppState::~AppState() {

}

bool AppState::isInit() const {
	return mIsInit;
}
void AppState::setInit(bool isInit) {
	mIsInit = isInit;
}

} /* namespace TileEngine */
