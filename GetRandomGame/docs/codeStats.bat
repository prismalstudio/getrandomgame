@echo off
::DIR ..\src\ *.h *.cpp /A-D /B /S > filelist.txt
ECHO Wait...
setlocal enableDelayedExpansion

for /R ..\src\ %%G in (*.h *.cpp) do findstr /R /N "^" %%G | find /C ":" >> tempCountFile.txt

for /f %%a in (tempCountFile.txt) do (
set text=%%a
set /a total+=!text:~0,10!
)

ECHO line count: %total%
DEL tempCountFile.txt
::findstr /R /N "^" %%G | find /C ":"
PAUSE