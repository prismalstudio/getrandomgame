@echo off
ECHO Wait...
setlocal enableDelayedExpansion

for /R ..\src\ %%G in (*.h *.cpp) do findstr /R /N "[^]" %%G | find /C ":" >> tempCountFile.txt

for /f %%a in (tempCountFile.txt) do (
set text=%%a
set /a total+=!text:~0,10!
)

ECHO line count without blank lines
ECHO in src dir: %total%
DEL tempCountFile.txt
::findstr /R /N "^" %%G | find /C ":"
PAUSE